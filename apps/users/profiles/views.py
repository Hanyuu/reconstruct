# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.views.generic import View
from django.template import RequestContext
from django.conf import settings
from django.contrib.auth.decorators import login_required
from apps.utils.helpers import cbv_decorator


@cbv_decorator(login_required)
class OfficeView(View):
    def get(self, request):
        profile = request.user.get_profile()
        return render_to_response('office.html', {'profile': profile}, RequestContext(request))


class IndexView(View):
    def get(self, request):
        return render_to_response('index.html', {'default_parent': settings.DEFAULT_PARENT, 'request':request}, RequestContext(request))

