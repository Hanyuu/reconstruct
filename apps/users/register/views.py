# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, resolve_url
from django.views.generic import View
from django.template import RequestContext
from django.contrib.auth.models import User
from django.forms.util import ErrorList
from django.conf import settings
from django.contrib.auth import authenticate, login

from apps.users.register.forms import RegistrationForm


class RegistrationView(View):
    def get(self, request, parent_id):
        if request.user.is_anonymous():
            reg_form = RegistrationForm()
            return render_to_response('register.html',
                                      {'reg_form': reg_form, 'default_parent': settings.DEFAULT_PARENT},
                                      RequestContext(request))
        else:
            return redirect(resolve_url('office'))

    def post(self, request, parent_id):
        reg_form = RegistrationForm(request.POST)
        error = False

        if reg_form.is_valid():
            data = reg_form.cleaned_data
            if data['register_or_login'] == '0': #register user
               reg_form.create_user(parent_id)
               return redirect(resolve_url('office'))

            elif data['register_or_login'] == '1': #auth user
                user = authenticate(username=reg_form.get_username_by_email(), password=data['password'])
                if user.is_active:
                    login(request, user)
                    return redirect(resolve_url('office'))
                else:
                    return render_to_response('account_blocked.html', {}, RequestContext(request))
            else:
                raise Exception('No such select in form!')
        else:
            return render_to_response('register.html',
                                      {'reg_form': reg_form, 'default_parent': settings.DEFAULT_PARENT},
                                      RequestContext(request))


