# Register your models here.
from django.contrib import admin

from apps.store.shop.models import Product, Order, UserProduct, UserCategory, UserSellProduct, Category


class ProductAdmin(admin.ModelAdmin):
    search_fields = ('name', 'price')


admin.site.register(Product, ProductAdmin)


class OrderAdmin(admin.ModelAdmin):
    search_fields = ('user__username', 'product__name')

    readonly_fields = ('user','product','status')


admin.site.register(Order, OrderAdmin)


class UserProductAdmin(admin.ModelAdmin):
    search_fields = ('user__username', 'product__name')

    readonly_fields = ('user', 'product', 'date_payed')
admin.site.register(UserProduct, UserProductAdmin)

class UserCategoryAdmin(admin.ModelAdmin):
    pass
admin.site.register(UserCategory, UserCategoryAdmin)

class UserSellProductAdmin(admin.ModelAdmin):
    pass
admin.site.register(UserSellProduct, UserSellProductAdmin)

class CategoryAdmin(admin.ModelAdmin):
    pass
admin.site.register(Category, CategoryAdmin)