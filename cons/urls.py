from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http import HttpResponse

from apps.users.profiles.urls import urlpatterns as profiles_urls
from apps.users.register.urls import urlpatterns as register_urls
from apps.construct.urls import urlpatterns as construct_urls
from apps.storage.urls import urlpatterns as storage_urls
from apps.store.shop.urls import urlpatterns as shop_urls
from apps.payments.payeer.urls import urlpatterns as payeer_urls


admin.autodiscover()


def return_code(request):
    return HttpResponse(u'4529511', content_type='text/plain')


urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^captcha/', include('captcha.urls')),
                       url(r'^payeer_4529511.txt', return_code), #For Payeer.com
)

urlpatterns += profiles_urls
urlpatterns += construct_urls
urlpatterns += storage_urls
urlpatterns += register_urls
urlpatterns += shop_urls
urlpatterns += payeer_urls

