# -*- coding: utf-8 -*-

import httplib2
import urllib
import anyjson
from django.conf import settings #PAYEER_API_PATH, PAYEER_API_KEY, PAYEER_API_ID, PAYEER_ACCOUNT, PAYEER_API_HOST

class PayerException(Exception):
    pass

class PayeerClient:
    auth = False
    language = 'ru'

    def __init__(self):
        params = {
            'account': settings.PAYEER_ACCOUNT,
            'apiId': settings.PAYEER_API_ID,
            'apiPass': settings.PAYEER_API_KEY
        }

        data = self.send_request(params)

        if data['auth_error'] == '0':
            self.auth = params
        elif data['auth_error'] == 'Please try later':
            raise PayerException('Please try later')
        else:
            raise PayerException('Auth error')


    def getPaySystems(self):
        if self.auth:
            url_data = {
                'action': 'getPaySystems',
            }
            return self.send_request(url_data)
        else:
            raise PayerException('Not Auth')
    '''
       'curIn' => 'USD', // счет списания
		'sum' => 1, // Сумма получения
		'curOut' => 'RUB', // валюта получения
		'to' => 'mail@mail.ru', // Получатель (email)
		//'to' => '+01112223344',  // Получатель (Телефон)
		//'to' => 'P1000000',  // Получатель (Номер счета)
		//'comment' => 'Текст комментария',
		//'anonim' => 'Y', // анонимный перевод
		//'protect' => 'Y', // протекция сделки
		//'protectPeriod' => '3', // период протекции (от 1 до 30 дней)
		//'protectCode' => '12345',
    '''
    def transfer(self, summ, to, curIn=u'USD', curOut=u'USD', comment=u'Перевод'):
        if self.auth:

            url_data = {
                'action': 'transfer',
                'sum': summ,
                'curIn': curIn,
                'curOut': curOut,
                'to': to
            }

            data = self.send_request(url_data)
            hid = data.get('historyId', None)
            if data['errors']:
                raise PayerException(data['errors'])
            elif hid is not None:
                return hid
            else:
                raise PayerException('Unknown state!\n %s'% data)
        raise PayerException('Not Auth')

    def get_balance(self):
        if self.auth:

            url_data = {
                'action': 'balance',
            }

            return self.send_request(url_data)
        raise PayerException('Not Auth')


    def send_request(self, params):
        conn = httplib2.HTTPSConnectionWithTimeout(settings.PAYEER_API_HOST, disable_ssl_certificate_validation=True)
        params.update({'language': self.language})
        if self.auth:
            params.update(self.auth)
        params = urllib.urlencode(params)
        conn.request('POST', settings.PAYEER_API_PATH, params, headers={'Accept-Encoding': '*/*', 'Content-Type': 'application/x-www-form-urlencoded',})
        response = conn.getresponse()
        data = response.read()
        conn.close()
        return anyjson.loads(data)

    def set_lng(self,lng):
        self.language=lng


def test():
    p = PayeerClient()

    print p.transfer(u'0.1',u'ryabovser@gmail.com')