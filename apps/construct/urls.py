# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.construct.views import ConsView

urlpatterns = patterns('',
                       url(r'construct/$', ConsView.as_view(), name='construct')
)