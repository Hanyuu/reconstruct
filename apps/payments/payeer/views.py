# -*- coding: utf-8 -*-
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, redirect, resolve_url
from django.template import RequestContext
from apps.utils.helpers import cbv_decorator
from apps.payments.payeer.utils import generate_sign
from django.conf import settings
from apps.store.shop.models import Order
import base64
from apps.payments.payeer.models import PayeerResponse
from dateutil import parser
from django.http import HttpResponse

@cbv_decorator(csrf_exempt)
class FailedView(View):
    def get(self,request):
        return render_to_response('failed.html',RequestContext(request))

@cbv_decorator(csrf_exempt)
class SuccessView(View):


    def get(self,request):
        ''''
        <QueryDict: {u'lang': [u'ru'], u'm_operation_id': [u'4592021'], u'm_sign': [u'232A670D579F4A41F0157964A31F7023E6AF560F41CD6FAC9ADDC803AD09065C'], u'm_orderid':
[u'5'], u'm_operation_date': [u'09.01.2014 12:52:15'], u'm_operation_ps': [u'2609'], u'm_amount': [u'0.01'], u'm_desc': [u'0JfQsNC60LDQtyDihJY1'], u'm_status':
[u'success'], u'm_operation_pay_date': [u'09.01.2014 12:52:25'], u'm_shop': [u'4529511'], u'm_curr': [u'USD']}>
        '''
        m_operation_id = request.GET.get('m_operation_id')
        m_operation_ps = request.GET.get('m_operation_ps')
        m_sign = request.GET.get('m_sign')
        m_orderid = request.GET.get('m_orderid')
        m_amount = request.GET.get('m_amount')
        m_desc = request.GET.get('m_desc')
        m_status = request.GET.get('m_status')
        m_operation_pay_date = request.GET.get('m_operation_pay_date')
        m_operation_date = request.GET.get('m_operation_date')
        m_shop = request.GET.get('m_shop')
        m_curr = request.GET.get('m_curr')

        payeeres = PayeerResponse(operation_id=m_operation_id, amount=m_amount, status=m_status, operation_date=parser.parse(m_operation_date))

        order = None
        try:
            order = Order.objects.get(pk=m_orderid)
        except:
            return render_to_response('order_not_found.html', RequestContext(request))

        payeeres.order = order

        plain_sign = u'%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s' % (m_operation_id, m_operation_ps, m_operation_date, m_operation_pay_date, m_shop, m_orderid, m_amount, m_curr, m_desc, m_status, settings.PAYEER_SHOP_KEY)

        sign = generate_sign(plain_sign)

        if m_status == 'success' and sign == m_sign:
            order.pay()
            payeeres.status = 1
            payeeres.save()
            return redirect(resolve_url('office'))
        else:
            payeeres.status = 2
            payeeres.save()
            return render_to_response('failed.html', RequestContext(request))


    def post(self,request):
        print request.POST

@cbv_decorator(csrf_exempt)
class StatusView(View):
    '''
    <QueryDict: {u'm_operation_id': [u'4619072'], u'm_sign': [u'DC02B109320F74EA5854424C633947570169CB88DBA85A97961F9E9D471E98C0'], u'm_orderid': [u'2'], u'm_operat
ion_date': [u'10.01.2014 13:22:46'], u'm_operation_ps': [u'2609'], u'm_amount': [u'0.02'], u'm_desc': [u'0JfQsNC60LDQtyDihJYy'], u'm_status': [u'success'], u'm_
operation_pay_date': [u'10.01.2014 13:22:55'], u'm_shop': [u'4529511'], u'm_curr': [u'USD']}>
    '''
    def post(self,request):

        m_orderid = request.POST.get('m_orderid')
        order = None
        try:
            order = Order.objects.get(pk=m_orderid)
        except:
            return HttpResponse(u'%s|error'% m_orderid, content_type='text/plain')
        if order is not None and order.status == 2:
            return HttpResponse(u'%s|success'% m_orderid, content_type='text/plain')
        else:
            return HttpResponse(u'%s|error'% m_orderid, content_type='text/plain')
