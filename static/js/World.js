/**
 * @author Hanyuu
 * Class provides rollback changes and rollback-rollback changes.
 */
var World=new function(){
    
    /**
     * Constructor
     */
    function World(gui){
        this.gui=gui;
        this.rollBackArray=[];
        this.rollForwardArray=[];
        var self=this;
        EventEmitter.getInstance().on('contentChanged', function(){

            if(self.rollBackArray.length==16)
                self.rollBackArray.shift();
            self.rollBackArray.push(Geometry.toJSON()); 
            self.rollForwardArray=[];
        }); 
        
    }
    /**
     * Roll back
     */
    World.prototype.rollBack=function(){
        var state=this.rollBackArray.pop();
        if(state){

            this.rollForwardArray.push(Geometry.toJSON());
            this.gui.clearField(false);
            Geometry.fromJSON(state);
        }
    }
    /**
     * Roll back rollbacked
     */
    World.prototype.rollForward=function(){
        var state=this.rollForwardArray.pop();

        if(state){
            this.rollBackArray.push(Geometry.toJSON());
            this.gui.clearField(false);
        
            Geometry.fromJSON(state);
        }
    }
    return World;
};