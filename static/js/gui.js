var GUI=new function(){
    var _instance;
    function GUI(){
        if(!_instance){
            var self=this;
            this.dialogOpened=false;
            this.lastEl=undefined;
            this.selectedEl=undefined; //Current selected EL.
            this.selectedElJQ=undefined; //Point to JQuery object of selected ElPanel object, cluge.
            this.id=new Date().getTime();
            this.groups=[];
            this.currentProject={};
            this.mailGroups=[];
            EventEmitter.getInstance().on('contentAdded', function(data){
                EventEmitter.getInstance().emit('contentChanged');
                var els=Geometry.currentMousePosition(data.e);
                if(els.length>0){
                    if(!self.loadingTemplate)
                        els[0].appendChildren(data.el,self);
                }
            
                $('input[placeholder], textarea[placeholder]').placeholder();
                Geometry.contentElements[data.el.JQueryEL.attr('id')]=data.el;
            
            });
        
            EventEmitter.getInstance().on('contentDeleted', function(data){
                delete Geometry.contentElements[data.JQueryEL.attr('id')];
            });
            _instance=this;
            this.loadMailGroups();
        }
        else{
            return _instance;
        }
        
    }
    GUI.getInstance=function(){
        return new GUI;
    }
    GUI.prototype.loadMailGroups=function(){
    }
    /**
     *  Will show  link for current project
     */
    GUI.prototype.showLink=function(){
        if(!this.currentProject.human_name){
            this.showMessage('Вы должны открыть проект.');
        }else if(!this.currentProject.server_name){
            this.publishProject();
        }else{
            $.tmpl($('#info-message'),{
                input:true,
                message:'http://'+userName+'.ruelsoft.org/'+this.currentProject.server_name+'/'
            }).showDialog();
        }
    }
    /**
     *  Create new template
     */
    GUI.prototype.createNew=function(){
        var self=this;
        this.showQuestion('Вы действительно хотите создать новый, пустой шаблон? Все не сохранённые данные будут потеряны!', function(){
            self.clearField(false);
        }); 
    };
    /**
     *  Will publish project 
     */
    GUI.prototype.publishProject=function(){
        var self=this;
        if(!self.currentProject.human_name){
            self.showMessage('Для публикации вы должны открыть проект.');
        }else if(self.currentProject.server_name){
            self.showQuestion('Вы действительно хотите перезаписать страницу '+self.currentProject.server_name+' ?',function(){
                self.saveProject({
                    humanName:self.currentProject.human_name, 
                    folder:self.currentProject.folder, 
                    exists:true, 
                    server_name:self.currentProject.server_name
                });
            })
        }else{
            var form=$.tmpl($('#page-name-question'));
            //Publish project, after all passed
            var publish=function(name){
                self.saveProject({
                    humanName:self.currentProject.human_name, 
                    folder:self.currentProject.folder, 
                    exists:true, 
                    server_name:name,
                    callback:function(){
                        form.remove();
                        if(self.currentProject.server_name)
                            self.showLink();
                    }
                });
            };
            form.fb('#save-btn','click',function(){
                if(self.currentProject.server_name){
                    self.showQuestion('Вы действительно хотите перезаписать страницу '+self.currentProject.server_name+' ?',publish);
                }else{
                    var name=form.find('#page-name-edit').val(); 
                    if(/^[a-z0-8_]{4,20}$/.test(name)){
                        publish(name);
                    }else{
                        self.showMessage('Имя может состоять только из английских букв или цифр.');
                    }
                }
            });
            form.showDialog();
        }
    };
    
    GUI.prototype.renameProject=function(id,name,callback){
        var self=this;
        $.post('/ajax/renameproject/',{
            id:id,
            new_name:name
        },function(data){
            if(data.code=='failed'){
                self.showMessage('Не удалось переименовать проект, обратитесь в тех.поддержку.<br/>Ошибка:'+data.info);
            }else{
                if(callback)
                    callback(data)
            }
        });  
    };
    /**
     *Open dialog
     */
    GUI.prototype.showOpenAndSaveDialog=function(save){
        var dialog=$.tmpl($('#test_open'),{
            save:save
        }),
        self=this,
        foldersNames=[],
        folders={};
        dialog.showDialog();

        //For search action
        dialog.fb('#do-search','click',function(){
            search($('#search-input').val());
        });
        dialog.fb('#search-input','keyup',function(e){
            if(e.keyCode==13)
                search($(this).val());
        });
        var search=function(text){
            
            self.findProjectsByName(text,function(data){
                dialog.find('.active').removeClass('active');
                makeFolder(data);
            });
            
        }
        //Main dir is active by default
        $('.main_dir').addClass('active');
        var trDBClickListener=function(e){
            removeInputs();
            //dblclick
            var target=$(e.target);
            if(target.attr('id')=='name'){
                var name=target.text();
                target.attr('old-name',name);
                target.text('')
                //Remove rename project input by click
                            
                $('<input id="rename-project" type="text" value="'+name+'"/>')
                .bind('keyup',function(e){
                    //On enter - rename project
                    var input=$(this);
                    if(e.keyCode==13){
                        var selected=$(dialog.find('#directs').find('.active')[0]).attr('id');
                        selected=selected!=undefined?selected:0;
                        for(var i in folders[selected]){
                            var project=folders[selected][i];
                            if(project.human_name==input.val()){
                                self.showMessage('Проект с таким иминем уже существует в выбранной папке.');
                                input.remove();
                                target.text(name)
                                return;
                            }
                        }
                        self.renameProject(target.parent().attr('id'),input.val(),function(){
                            target.text(input.val());
                            input.remove();
                        });
                    }
                })
                .appendTo(target)
                .focus();
                            
            }
        }
        var removeInputs=function(){
            $('#file_list').find('input[type="text"]').each(function(){
                $(this).parent().text($(this).parent().attr('old-name'));
                $(this).parent().removeAttr('old-name');
                $(this).remove();
            });
        }
        
        var openAndPublish=function(name,id){
            var open=function(){
                self.loadProjectById(id,function(){
                    dialog.remove();
                    self.publishProject();
                            
                });                               
            };
            if(self.currentProject.human_name)
                self.showQuestion('Вы действительно желаете открыть и опубликовать проект '+name+' ?<br/> При этом все не сохранённые данные открытого проекта будут потеряны.',open)
            else
                open();
        };
        dialog.fb('#publish-project','click',function(){
            var selected=dialog.find('#file_list').find('.active');
            openAndPublish(selected.find('#name').text(),selected.attr('id'));
        });
        var trClickListener=function(e){
            var target=$(e.target);
            if(!target.is(':input'))
                removeInputs();
            $('.right').find('.active').removeClass('active');
            $(this).addClass('active');
            if(target.hasClass('publish')){
                var tr=$(this)
                openAndPublish(tr.find('#name').text(),tr.attr('id'))
            }                        
        };
        
        /**
         *  Create table sorting
         **/
        var sotr=function(el,by){
            var trs=dialog.find('#file_list').find('tr')
            ,reverse=false;
            if(el.attr('reverse')=='true'){
                el.removeAttr('reverse');
                reverse=true;
            }else{
                el.attr('reverse','true')
            }
            trs=trs.sort(function(a,b){
                a=$(a);
                b=$(b);
                var i=0;
                switch(by){
                    case 'by-name':
                        var txt1=$(a.find('td')[0]).text();
                        var txt2=$(b.find('td')[0]).text();
                        if ((txt1 < txt2)){
                            return reverse?1:-1;
                        }else if ((txt1 > txt2)&&!reverse){
                            return  reverse?-1:1;
                        }else{
                            return 0;
                        }
                        break;
                    case 'by-link':
                        if(reverse)
                            return $(a.find('td')[1]).text().length-$(b.find('td')[1]).text().length
                        else
                            return $(b.find('td')[1]).text().length-$(a.find('td')[1]).text().length
                        break;
                    case 'by-date':
                        var aDate=new Date(parseInt($(a.find('td')[2]).attr('data-date'))*1000)
                        ,bDate=new Date(parseInt($(b.find('td')[2]).attr('data-date'))*1000);
                            return (aDate>bDate)&&(reverse)?-1:1;
                        break;
                }
                
            });
            dialog.find('#file_list').find('tr').remove();
            trs.bind('click',function(e){
                trClickListener.call(this,e);
            });
            trs.bind('dblclick',function(e){
                trDBClickListener.call(this,e);
            });
            trs.appendTo(dialog.find('#file_list')).show(100);
            
        
        }
        
        //Sort by name
        dialog.fb('#by-name','click',function(){
            sotr($(this),'by-name');
        }).css('cursor','pointer');
        //Sort by link
        dialog.fb('#by-link','click',function(){
            sotr($(this),'by-link');
        }).css('cursor','pointer');
        //Sort by date
        dialog.fb('#by-date','click',function(){
            sotr($(this),'by-date');
        }).css('cursor','pointer');
        if(save&&$(document).find('.page_name').text().indexOf('Безымянный проект')==-1)
            dialog.find('#project-name').val($(document).find('.page_name').text());
        var width=parseInt($('.dialog').css('width'));
        $('.dialog').show()
        $('.dialog').load(function(){
            $('.dialog').css('margin-left',-(Math.ceil(width/2))); 
        });
        
        //Load folder list
        var loadFolders=function(callback){
            $.post('/ajax/getfolders/',function(data){
                if(data.code=='failed'){
                    self.showMessage('Не удалось загрузить список папок обратитесь в служду тех.поддержки<br/>Описание:'+data.info);
                }else{
                    dialog.find('#directs').find('.folder').remove();
                    foldersNames=[];
                    data.map(function(folder){
                        
                        $('<a class="folder" href="#" id='+folder.id+'><span>'+folder.name+'('+folder.count+')</span></a>')
                        .bind('click',function(){
                            $('.active').removeClass('active');
                            $(this).addClass('active');
                            dialog.find('#file_list').find('tr').remove();
                            loadFolder(folder.id);
                        })
                        .appendTo(dialog.find('#directs')); 
                        foldersNames.push(folder)
                    });
                    $('.scroller .s_body').each(function(){
                        if($(this).parent().height()< $(this).height()){
                            $(this).parent().css('overflow-y', 'scroll');
                        }
                    });
                    if(callback)
                        callback();
                }
            },'json');
        }
        loadFolders();
        
        //Load folder from server
        var loadFolder=function(id){
            $.post('/ajax/getfoldercontent/',{
                folder:id
            },function(data){
                if(data.code=='failed'){
                    self.showMessage('Не удалось загрузить папку '+id+', обратитесь в служду тех.поддержки<br/>Описание:'+data.info);
                }else{
                    folders[id]=data.projects;
                    //Shared folder cluge
                    /*if(id==480){
                        dialog.find('.button_navig').find('a').hide();
                    }else{
                        dialog.find('.button_navig').find('a').show();
                    }*/
                    makeFolder(data);
                }
            },'json');
        };
        //Create folder content
        var makeFolder=function(data){
            var list=dialog.find('#file_list');

            dialog.find('#file_list').find('tr').remove();

            var names=[];
            for(var i in data.projects){
                var el=data.projects[i];
                names.push(el.human_name)
                if(el){
                    $('#open-dialog-item').tmpl({
                        el:el
                    }).appendTo(list).bind('click',function(e){
                        trClickListener.call(this,e);
                    }).bind('dblclick',function(e){
                        trDBClickListener.call(this,e);
                    });
                }
            }
            dialog.find('#project-name').autocomplete({
                zIndex: 9999,
                source: names
            });
        };
        //Load default folder
        loadFolder(0);
        dialog.fb('.main_dir','click',function(){
            $('.active').removeClass('active');
            $(this).addClass('active');
            loadFolder(0);
        });
        //Proseed logic attr in A-tags
        var proseedLogic=function(el,proseedEl){

            switch(el.attr('logic')){
                //Just close window
                case 'close':
                case 'cancel':
                    proseedEl.remove();
                    break;
                //Save project
                case 'save-template':
                    var name=dialog.find('#project-name').val();
                    if(name.length>1){
                        //Find project by name
                        var currentFolder=$('#file_list').find('tr'),
                        currentProject,
                        diskName=undefined,
                        folder=$(dialog.find('#directs').find('.active')[0]).attr('id')?$(dialog.find('#directs').find('.active')[0]).attr('id'):0;
                        currentFolder.each(function(){
                            var el=$(this);
                            if(el.find('#name').text().indexOf(name)>-1)
                                currentProject=el;
                        });
                        if(currentProject){
                            diskName=currentProject.find('#disk-name').text().trim().split('\/');
                            if(diskName.length&&diskName[3]!=undefined)
                                diskName=diskName[3];
                        }
                        //Dont rewrite in disk if no files in disk
                        if(/Опубликовать/.test(diskName)){
                            diskName=undefined;
                        }
                        if(currentProject){
                            self.showQuestion('Вы действительно хотите перезаписать '+name+' ?',function(){
                                self.saveProject({
                                    humanName:name,
                                    folder:folder,
                                    exists:true,
                                    server_name:diskName,
                                    callback:function(){
                                        loadFolders(loadFolder(folder))
                                    }
                                });
                            });
                        }else{
                            self.saveProject({
                                humanName:name,
                                folder:folder,
                                exists:true,
                                callback:function(){
                                    loadFolders(loadFolder(folder))
                                }
                            });
                        }
                    }else{
                        self.showMessage('Введите имя проекта');
                    }
                    break;
                //Create folder
                case 'ok':
                    var name=proseedEl.find('.d_input').val();
                    if(name.length<4){
                        self.showMessage('Имя папки должно быть не меньше 4х символов');
                    }else{
                        $.post('/ajax/createfolder/',{
                            folderName:name
                        },function(data){
                            if(!data||data.code=='failed')
                                return;
                            folders[data.id]=data;
                            foldersNames.push(data);
                            $('<a href="#" id='+data.id+' class="folder"><span>'+data.name+'(0)</span></a>')
                            .bind('click',function(){
                                $('.active').removeClass('active');
                                $(this).addClass('active');
                                dialog.find('#file_list').find('tr').remove();
                                
                            })
                            .appendTo(dialog.find('#directs'));
                            proseedEl.remove();
                        },'json')
                    }
                    break;
                //Open project
                case 'open-template':
                    var tmpl=$($('#file_list').find('.active'));
                    if(tmpl){
                        var open=function(){
                            self.loadProjectById(tmpl.attr('id'),function(){
                                dialog.remove();
                            });                               
                        };
                        if(self.currentProject.human_name)
                            self.showQuestion('Вы действительно желаете открыть проект '+tmpl.find('#name').text()+' ?<br/>При этом все не сохранённые данные открытого проекта будут потеряны.',open);
                        else 
                            open();
                    }
                    break;
                case 'folder':
                    proseedEl.find('.active').removeClass('active');
                    el.addClass('active');
                    break;
                //Delete project
                case 'delete-project':
                    var ids=[],
                    names=[],
                    selected=$('#file_list').find('.active');
                    //For mass delet project, not used, but may be used in future
                    selected.each(function(){
                        var el=$(this);
                        ids.push(el.attr('id'));
                        names.push(el.find('#name').text());
                    });

                    self.showQuestion('Вы действительно желаете удалить '+names.join(',')+' ?',
                        function(){
                            self.deleteProjectsById(ids,function(data){
                                dialog.find('#select-all').removeAttr('checked');
                                selected.hide(400,function(){
                                    selected.remove();
                                });  
                            });                               
                        });
                    break;
                case 'rename-project':
                    var nameDialog=$('#name-tmpl').tmpl({
                        question:'Введите название проекта'
                    });
                    nameDialog.showDialog();
                    
                    nameDialog.fb('a','click',function(){
                        var el=$(this);
                        switch(el.attr('logic')){
                            case 'close':
                            case 'cancel':
                                nameDialog.remove();
                                break;
                            case 'ok':
                                var name=nameDialog.find('.d_input').val();
                                if(name.length<4){
                                    self.showMessage('Имя проекта должно быть не меньше 4х символов');
                                }else{
                                    var tmpl=$($('#file_list').find('.active'));
                                    if(tmpl.attr('id')){
                                        self.renameProject(tmpl.attr('id'),name,function(){
                                            nameDialog.remove();
                                            tmpl.find('#name').text(name);
                                        });
                                    }
                                    
                                }
                                
                        }
                    });
                    break;
            }
        }
        
        dialog.fb('a','click',function(){
            proseedLogic($(this),dialog);
        });
        //Create folder
        dialog.fb('.create','click',function(){
            var nameDialog=$('#name-tmpl').tmpl({
                question:'Придумайте для название папки',
                title:'Создание папки'
            });
            nameDialog.showDialog().fb('a','click',function(){
                proseedLogic($(this),nameDialog);
            });
            
        });
        //Rename folder
        dialog.fb('.rename','click',function(){
            var nameDialog=$('#name-tmpl').tmpl({
                title: 'Переименование папки',
                question:'Введите имя папки'
            });
            nameDialog.showDialog().fb('a','click',function(){
                switch($(this).attr('logic')){
                    case 'ok':
                        var name=$('.d_input').val();
                        var selected=$(dialog.find('#directs').find('.active')[0]);
                        if(selected.attr('id')){
                            if(name.length<4){
                                self.showMessage('Имя папки должно быть не меньше 4х символов');
                            }else{
                                $.post('/ajax/renamefolder/',
                                {
                                    name:name,
                                    id:selected.attr('id')
                                },function(data){
                                    if(data.code=='failed'){
                                        self.showMessage('Ошибка при переименовании папки, обратитесь в тех.поддержку.<br/>Описание'+data.info); 
                                    }else{
                                        loadFolders(function(){
                                            dialog.find('#directs').find('#'+selected.attr('id')).addClass('active');
                                        });
                                        nameDialog.remove();
                                    }
                                },'json');
                            }
                        }else{
                            self.showMessage('Вы должны выбрать папку');
                        }
                        break;
                    case 'cancel':
                        nameDialog.remove();
                        break;
                }
            });
        });
        //Delete folder
        dialog.fb('.delete','click',function(){
            var selected=$(dialog.find('#directs').find('.active')[0]);
            if(selected.attr('id')){
                self.showQuestion('Вы действительно хотите удалить папку '+selected.text()+' ?',function(){
                    $.post('/ajax/deletefolder/',
                    {
                        id:selected.attr('id')
                    },function(data){
                        if(data.code=='failed'){
                            self.showMessage('Ошибка при удалении папки, обратитесь в тех.поддержку.<br/>Описание'+data.info); 
                        }else{
                            selected.remove();
                            loadFolders(function(){
                                loadFolder(0);
                            });
                        }
                    },'json');
                });
            }else{
                self.showMessage('Вы должны выбрать папку');
            }
        });
        //Move project
        dialog.fb('.remove','click',function(){
            var selected=$('#file_list').find('.active');
            var projects=[];
            selected.each(function(){
                projects.push({
                    id:$(this).attr('id'),
                    human_name:$(this).find('#name').text()
                })
            });

            var moveDialog=$('#move-projects')
            .tmpl({
                folders:foldersNames,
                projects:projects
            })            
            .showDialog();
            
            moveDialog.fb('a','click',function(){
                proseedLogic($(this),moveDialog,projects);
            })
            .fb('.move-projects','click',function(){
                var folderId=parseInt(moveDialog.find('.active').attr('id'));
                $.post('/ajax/moveprojects/',
                {
                    projects:projects,
                    new_folder:folderId
                },
                function(data){
                    dialog.find('#select-all').removeAttr('checked');
                    if(data.code=='failed'){
                        self.showMessage('Ошибка при перемещении папок, обратитесь в тех.поддержку.<br/>Ошибка:'.data.info);
                    }else{
                        loadFolders(function(){
                            dialog.find('#directs').children('#'+folderId).addClass('active');
                            loadFolder(folderId);
                            moveDialog.remove();
                        });
                        
                    }   
                },'json')
                
            });   
        });
    };
    
    GUI.prototype.findProjectsByName=function(name,callback){
        var self=this;
        $.post('/ajax/findprojects/',{
            name:name
        },function(data){
            if(data.code=='failed'){
                self.showMessage('Не удалось произвести поиск, обратитесь в службу тех.поддержки.<br/>'+data.info)
            }
            else{
                callback(data);
            }
        },'json')
    }
    /**
     *  Will save active project
     */
    GUI.prototype.saveProject=function(params){
        //humanName,folder,exists,diskName
        var self=this,
        //Simple data
        data={
            name:params.humanName,
            json:Geometry.toJSON(),
            folder:params.folder
        };
        //If we try to create file in hosting - send file name & html content
        if(params.server_name){
            data.server_name=params.server_name;
            data.html=self.generateHTML(Geometry.contentElements,false);
        }
        $.ajax({
            type:'POST',
            enctype:'multipart/form-data',
            dataType:'json',
            url:'/ajax/saveproject/',
            data:data
        }).done(function(data){
            
            var doc=$(document);
            
            if(data.code=='failed'){
                self.showMessage('Не удалось сохранить проект, обратитесь в тех.поддержку.<br/>Ошибка:'+data.info);
            }else if(data.diskResult.code=='failed'){
                if(data.diskResult.info=='exists'){
                    self.showMessage('Не удалось сохранить проект под этим иминем, такое имя уже используется у проекта '+data.diskResult.project);
                }else{
                    self.showMessage('Не удалось записать проект на диск, обратитесь в тех.поддержку.<br/>Ошибка:'+data.diskResult.info);
                }
            }else{
                //Set current project
                self.currentProject={
                    human_name:params.humanName,
                    id:data.id,
                    folder:params.folder,
                    server_name:params.server_name
                };
                //View name
                $(document).find('.page_name').text(params.humanName);
                if(!params.exists){
                    $.tmpl($('#open-dialog-item'),{
                        el:{
                            id:data.id,
                            human_name:params.humanName,
                            server_name:data.diskResult.info.server_name,
                            last_edit:(new Date).getTime()
                        }
                    }).appendTo(doc.find('#file_list')); 
                }else{
                    if(data.diskResult.code=='ok'){
                        self.showMessage({
                            input:true,
                            message:'http://'+data.diskResult.server_name
                        }).find('input').select();
                    }else if(data.diskResult.code=='limit-expired'){
                        self.showMessage('Проект сохранён, но <span style="color:red">не опубликован</span> У вас закончилдось место на хостинге.<br/>Вы опубликовали <span style="color:red">'+data.diskResult.published+'</span> страниц.<br/>Максимальное число проектов которые вы можете опубликовать <span style="color:red">'+data.diskResult.max_pages+'</span>');
                        return;
                    }else{
                        self.showMessage('Сохранено');
                    }
                }
                doc.find('#project-name').val('');
            }
            if(params.callback)
                params.callback();
        });
    }
    /**
     * Will delete project by id
     */
    GUI.prototype.deleteProjectsById=function(ids,callback){
        var self=this;
        if(this.currentProject.id in ids){
            this.showMessage('Вы не можете удалить открытый проект');
        }else{
            $.post('/ajax/deleteprojects/',{
                projects:ids
            },function(data){
                if(data.code=='failed'){
                    self.showMessage('Не удалось удалить проекты, обратитесь в тех.поддержку.<br/>Ошибка: '+data.info);
                }
                if(callback)
                    callback(data)
            
            },'json');
        }
    };

    /**
*   Wilss save CURRENT PROJECT
*/
    GUI.prototype.saveCurrentProject=function(){
        var self=this;
        this.showQuestion('Вы действительно хотите перезаписать проект '+self.currentProject.human_name+' ?', function(){
            if(self.currentProject.id&&self.currentProject.human_name){
                self.saveProject({
                    humanName:self.currentProject.human_name, 
                    folder:self.currentProject.folder, 
                    exists:true, 
                    server_name:self.currentProject.server_name
                });
            }
        });
    };
    /**
     *  Will load project by ID
     */
    GUI.prototype.loadProjectById=function(id,callback){
        var self=this;
        $.post('/ajax/getproject/',{
            id:id
        },function(data){
            if(data.code=='failed'){
                self.showMessage('Не удалось загрузить проект '+id+', Обратитесь в тех.поддержку.<br/>Описание ошибки:'+data.info);
            }else{
                self.clearField(false);
                Geometry.fromJSON(data.project['content']);
                self.currentProject=data.project;
                $('.page_name').text(data.project.human_name);
                if(callback)
                    callback(data);
            }
        },'json');
    }
    /*
     *Resize elements
     */
    GUI.prototype.resizer=function(el,e){
        var cords=el.getCords(),
        condition=null, skiped=false;
        if(typeof e.ctrlKey!='undefined'){
            condition=e.keyCode;
        }else{
            condition=e['condition'];
            e['ctrlKey']=true;
                
        }
        switch(condition){
            case 'center-top':
            case 38: //UP
                if(e.ctrlKey&&el.el.resizable){
                    var t=el.JQueryEL.position().top;
                    el.JQueryContent.css({
                        'height':(parseInt(el.JQueryContent.css('height'))+(e.altKey?-1:+1))+'px'
                    });
                    el.JQueryEL.css({
                        'height':(parseInt(el.JQueryEL.css('height'))+(e.altKey?-1:+1))+'px',
                        'top':t+(e.altKey?+1:-1)
                    });

                }else{
                    --cords.y;
                }
                break;
            case 'center-bottom':
            case 40: //DOWN
                if(e.ctrlKey&&el.el.resizable){
                    el.JQueryContent.css('height',(parseInt(el.JQueryContent.css('height'))+(e.altKey?-1:+1))+'px');
                    el.JQueryEL.css('height',(parseInt(el.JQueryEL.css('height'))+(e.altKey?-1:+1))+'px');
                }else{
                    ++cords.y;
                }
                break;
            case 'middle-right':
            case 39: //LEFT
                if(e.ctrlKey&&el.el.resizable){
                    el.JQueryContent.css('width',(parseInt(el.JQueryContent.css('width'))+(e.altKey?-1:+1))+'px');
                    el.JQueryEL.css('width',(parseInt(el.JQueryEL.css('width'))+(e.altKey?-1:+1))+'px');
                }else{
                    ++cords.x;
                }
                break;
            case 'middle-left': 
            case 37: //right
                if(e.ctrlKey&&el.el.resizable){
                    var l=el.JQueryEL.position().left;
                    el.JQueryContent.css('width',(parseInt(el.JQueryContent.css('width'))+(e.altKey?-1:+1))+'px');
                    el.JQueryEL.css({
                        'width':(parseInt(el.JQueryEL.css('width'))+(e.altKey?-1:+1))+'px',
                        'left':l+(e.altKey?+1:-1)
                    });
                }else{
                    --cords.x;
                }
                break;
            default:
                skiped = true;
                break;
               
        }

        if(e.ctrlKey){
        //WTF where              
        }else{
            if(Geometry.isElementsContains(el.JQueryEL,$('.panel-content')))
                el.move(cords.x,cords.y); 
        }
        if(!skiped){
            return false;
        }
        
    };
    /**
     *  Click handler
     */
    GUI.prototype.onMouseClick=function(e){
        var doc=$(document);
        var self=this;
        
        if($(e.target).hasClass('elem_li_active'))
            return false;
        doc.unbind('keydown');
        doc.unbind('keypress');
        
        if((!$(e.target).hasClass('tuker')&&$(e.target).parents('.color_list').length==0)){
            
            $(document.body).find('.tuker').each(function(){
                $(this).removeClass('select');
            });
            $(document.body).find('.tuker').find('.main, .other, .butts').show();
            $(document.body).find('.tuker').find('#colors-selector').hide();
            
        }
        if($('.menu-edit').is(':visible'))
            $('.menu-edit').hide(200); 
         
        var el=$(e.target);
        el.parents('div').each(function(){
            var id=$(this).attr('id');
            if(!isNaN(parseInt(id)))
                el=Geometry.contentElements[id];
        });
        if(!el)
            return;//??
        if(!el.JQueryEL)
            el=Geometry.contentElements[el.attr('id')];
        if(el==undefined||el.getCords==undefined)
            return;
   
        doc.bind('keydown',function(e){
            return self.resizer(el, e);
  
        });
    };
    /**
     * Mouse move handler
     */
    GUI.prototype.onMouseMove=function(e){
        if(this.dialogOpened)
            return;
        var el=$(e.target);
        if(el.hasClass('menu-edit')
            ||el.hasClass('edit')
            ||el.hasClass('delete')){
            return;
        }  
        if(this.selectedEl){
            $('.tool-tip').remove();
            e.preventDefault?e.preventDefault():e.returnValue = false;
            switch(this.selectedEl.ObjectType){
                case 'ElPanel':
                    this.moveElPanel(e);
                    break;
                case 'ElAppended':
                    this.moveElAppended(e);
                    break;
                default:
                    throw 'Unknown EL Object: '+this.selectedEl.ObjectType;
                    break;
            }
                
        }else{

            if(el.attr('data-tool-tip')&&
                !($.cookie('tool-tips-off')&&$.cookie('tool-tips-off')!='false')){

                if($('.tool-tip').text().indexOf(el.attr('data-tool-tip'))!=-1)
                    return;
                $('.tool-tip').remove();
                $('<div class="tool-tip">'+el.attr('data-tool-tip')+'</div>')
                .css({
                    left:e.pageX+20,
                    top:e.pageY+5,
                    'z-index':999999
                })
                .appendTo($(document.body))
            }else{
                $('.tool-tip').remove();
            }
            var _el=Geometry.currentMousePosition(e);

            $('.menu-edit').hide();
            if(_el[0])
                _el[_el.length-1].showMenu();
        }
    };
    /**
     *  Load user groups from mailsender
     */
    GUI.prototype.loadMailsenderGroups=function(){

    };    
    /**
     *  Show usefull links menu
     */
    GUI.prototype.showHelp=function(){
        var form=$.tmpl($('#help'));
        form.appendTo('body');
        form.find('.window_support').css({
            'margin-top':(parseInt(form.find('.window_support').css('height'))/-2)+'px',
            'z-index':13000
            
        });
        form.find('.wrap').css({
            'z-index':12000
        });
        form.fb('.close','click',function(){
            form.remove();
        });


        form.fb('#video-help','click',function(){
            form.remove();
            var help_list = $.tmpl($('#video-help-list'));
            help_list.showDialog();
            $('div.window_support li a').unbind('click').bind('click', function(e){
                help_list.remove();
                $.tmpl($('#video-help-tmpl'), {
                    href:$(e.target).attr('href')
                }).showDialog(); 
                return false;

            });

        //            $.tmpl($('#video-help-tmpl')).showDialog();

        });


    };
    /**
     * MouseUP handler
     */
    GUI.prototype.onMouseUp=function(e){

        if(this.dialogOpened)
            return;
            
        if(this.selectedEl){
            switch(this.selectedEl.ObjectType){
                case 'ElPanel':
                    if(Geometry.containsCords(e,$('.panel-content'))&&
                        !Geometry.containsCords(e,$('.instruments')))
                        this.selectedEl.onDrop(e);
                    if(this.selectedElJQ)
                        this.selectedElJQ.remove();
                    this.selectedEl=null;
                    this.selectedElJQ=null;
                    break;
                case 'ElAppended':
                    this.selectedEl.mouseUpHandler(e,this);
                    this.selectedEl=null;  
                    break;
                default:
                    throw 'Unknown EL Object: '+this.selectedEl.ObjectType;
                    break;
            }
                
        }
    };

    /***
     *Informate user
     */
    
    GUI.prototype.showMessage=function(msg){
        var form_info=$.tmpl($('#info-message'),{
            
            message:typeof msg == 'object'?msg.message:msg,
            input:typeof msg == 'object'?msg.input:false
        });
        form_info.showDialog();
        return form_info;
    };
    
    GUI.prototype.clearField=function(askUser){
        var self=this;
        var clear=function(){
            for(var i in Geometry.contentElements){
                var el=Geometry.contentElements[i];
                el.JQueryEL.remove();
                $('.elem_content').find('#panel-'+el.id).parent().remove();
            }
            
            Geometry.contentElements={};
            
            $('.panel-content').css('background','transparent');
            $('.page_name').text('Безымянный проект');
            $('.panel-content').attr('data-keywords','');
            $('.panel-content').attr('data-trap','');
            $('.panel-content').attr('data-trap-active','');
            self.currentProject={};
        }
        if(askUser){
            this.showQuestion('Вы действительно хотите очистить рабочую область? Все не сохранённые данные будут потеряны.',function(){
                clear();
            });
        }else{
            clear();
        }
    }
    GUI.prototype.generateHTML=function(elements,excape){
        //FUCK!
        var html='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><title>'
        +($('.panel-content').attr('data-title')==undefined?$('.page_name').text():$('.panel-content').attr('data-title'))+
        '</title>'+
        '<link type="image/x-icon" rel="shortcut icon" href="'+($('.panel-content').attr('data-favicon')==undefined?'http://ruelsoft.com/st/img/favicon.ico':$('.panel-content').attr('data-favicon'))+'">'+
        '<link type="text/css" rel="stylesheet" media="screen" href="http://capture-sys.ruelsoft.org/css/ui-lightness/jquery-ui-1.8.17.css">'+
        '<link type="text/css" rel="stylesheet" media="screen" href="http://capture-sys.ruelsoft.org/css/capture-sys.css?1">'+
        '<script src="http://capture-sys.ruelsoft.org/js/jquery/jquery-1.7.js" type="text/javascript"></script>'+
        '<script src="http://capture-sys.ruelsoft.org/js/jquery/jquery-ui-1.8.17.js" type="text/javascript"></script>'+
        '<script src="http://capture-sys.ruelsoft.org/js/jquery/jquery.placeholder.min.js" type="text/javascript"></script>'+
        '<script src="http://capture-sys.ruelsoft.org/js/jwplayer.js" type="text/javascript"></script>'+
        //For demo.appsruel.com we uses base.js, for dev.ruelsoft.org base2.js
        '<script src="http://capture-sys.ruelsoft.org/js/base'+(window.location.host.toString().indexOf('demo.appsruel.com')>-1?'.js?':'2.js?')+(new Date()).getTime()+'" type="text/javascript"></script>'+
        '<meta content="'+$('.panel-content').attr('data-keywords')+'" name="keywords">'+
        '</head>'+
        '<body style= \'margin:0;height:'+(parseInt($('.panel-content').css('height'))+12+parseInt($('.panel-content').css('border-bottom-width'))*2)+'px'+';padding:0;background-color:'+
        $('.panel-content').css('background-color')+';'+
        'background-attachment:fixed;'+
        'background-size: 100% 100%;'+
        'background-image:'+$('.panel-content').css('background-image')+
        ';\'>'+
        
        '<div style="margin: 6px 0 6px -'+(parseInt($('.panel-content').css('width'))/2)+'px;'+
        'position:relative;'+
        'left:50%;'+
        'width:'+$('.panel-content').css('width')+';'+
        'height:'+$('.panel-content').css('height')+';'+
        'border:'+$('.panel-content').css('border-bottom-style')+' '+$('.panel-content').css('border-bottom-width')+' '+$('.panel-content').css('border-bottom-color')+
        ';" >'+
        '<div style="display:none;" id="key">'+$('#key').text()+'</div>';
        var _p=$('.panel-content');
        if(_p.attr('data-trap-active')=='true'){
            html+='<div id="trap" style="top:0;left:0;position:absolute;width: 0%; height: 0%; border: 0px;"><iframe style="overflow:scroll;width: 100%; height: 100%; border: 0px;" src="'+_p.attr('data-trap')+'"></iframe></div>'
        }
        //-------------------------------------------------------
        //          Create clear-HTML from content el
        //-------------------------------------------------------
        var unpack=function(parent_cloned,pos){
            var el=parent_cloned.children('.wrap-el').children();
            el.css({
                'position':'absolute',
                'left':pos.left,
                'top':pos.top,
                'width':pos.width,
                'heigth':pos.heigth,
                'z-index':pos.zIndex
            });
            //For stupid chrome
            //Chrome cannot understand $.css('background-size : 100% 100%'), and will do 'background-size: 100%' insted.
            el.attr('style',el.attr('style').replace('background-size: 100%;','background-size : 100% 100%;'));
            return parent_cloned.children('.wrap-el').html();
            
        };
        
        for(var i in elements){
            var el=elements[i];
            var parent_clear=el.JQueryContent.parent().parent();
            var parent_cloned=parent_clear.clone();
            var pos={
                left:(parseInt(el.JQueryEL.css('left'))+15)+'px',
                top:(parseInt(el.JQueryEL.css('top'))+15)+'px',
                width:el.el.resizable?(
                    !parent_clear[0].style.width?parent_clear.css('width'):parent_clear[0].style.width
                    ):'auto',
                height:el.el.resizable?parent_clear.css('height'):'auto',
                zIndex:parseInt(parent_clear.css('z-index'))

            }

            if(el.el.unpack){
                html+=Prototypes.Unpacks[el.el.unpack].call(el,pos);
            }else{
                html+=unpack(parent_cloned,pos);
            }    
        }

        html+='</div></body></html>'
        if(excape)
            html=html.replace(/</g,'&lt;').replace(/>/g,'&gt;');
        return html;
    }
    /**
     * Ask user
     */
    GUI.prototype.showQuestion=function(msg,onOk,onCancel){
        var self=this;
        p(arguments.callee.caller);
        self.dialogOpened=true;
        var form=$.tmpl($('#questions'),{
            question:msg
        }).showDialog();
        
        form.find('#save-btn').bind('click',function(){
            if(onOk)
                onOk();
            form.remove();
            self.dialogOpened=false;
        });

        form.find('#close-btn,.d_close').bind('click',function(){
            if(onCancel)
                onCancel();
            self.dialogOpened=false;
            form.remove();
        });
        
    };
    GUI.prototype.setSelectedEl=function(e,el){
        this.selectedEl=el;
            
    };
    
    
    GUI.prototype.moveElAppended=function(e){
        
        var findedEL=Geometry.currentMousePosition(e)[0]
        ,currentEL=Geometry.contentElements[this.selectedEl.JQueryEL.attr('id')]; 
        if(findedEL&&findedEL.JQueryEL.attr('id')!=currentEL.JQueryEL.attr('id')){
            currentEL.JQueryEL.css({
                'z-index':parseInt(findedEL.JQueryEL.css('z-index'))+100
            });
            currentEL.moveCallback=function(){
                currentEL.JQueryEL.css({
                    'z-index':parseInt(findedEL.JQueryEL.css('z-index'))-100
                });
            }
        }else if(currentEL.moveCallback){
            currentEL.moveCallback();
            currentEL.moveCallback=undefined;
        }
        currentEL.move(
            e.pageX-this.selectedEl.mouseOffset.x,
            e.pageY-this.selectedEl.mouseOffset.y
            );
        
    };
   
    GUI.prototype.moveElPanel=function(e){
        if(this.selectedEl.JQueryEL
            .attr('id').indexOf('cloned')==-1){
            if(!this.selectedElJQ){
                this.selectedElJQ=this.selectedEl.JQueryEL.clone(true).css({
                    position:'absolute',
                    'z-index':99999
                }).appendTo($(document.body)); 
            }
               
        }
        this.selectedElJQ.css({
            left:(e.pageX-this.selectedEl.mouseOffset.x)+'px',
            top:(e.pageY-this.selectedEl.mouseOffset.y)+'px'
        });
    };
    
    GUI.prototype.onElSelected=function(data){
        var e=data.event,
        el=data.el;
        e.preventDefault?e.preventDefault():e.returnValue = false;
        switch(e.button){
            case 0:
            case 1:
                this.setSelectedEl(e,el);
                return false;
                break;
            case 2:
                return false;
                break;
        }
    };
    return GUI;
}
