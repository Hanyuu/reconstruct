# -*- coding: utf-8 -*-
import smtplib

from django import forms
from captcha.fields import CaptchaField
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import dns.resolver

from apps.users.profiles.models import Profile


class RegistrationForm(forms.Form):
    captcha = CaptchaField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    register_or_login = forms.ChoiceField(choices=((0, u'Регистрация'), (1, u'Вход')), widget=forms.RadioSelect())

    def create_user(self, parent_id):
        user = User(username=self.get_username_by_email(), email=self.cleaned_data['email'])
        user.set_password(self.cleaned_data['password'])
        user.save()
        profile = Profile(user=user)
        profile.parent = Profile.objects.get(pk=parent_id)
        profile.save()

        return (user, profile)

    def get_username_by_email(self):
        return self.cleaned_data['email'].split('@')[0]


    def clean_email(self):

        value = self.data['email']
        if User.objects.filter(email=value).count() > 0:
            raise ValidationError(u'Пользователь с такой почтой уже зарегистрирован!')
        elif User.objects.filter(username=self.get_username_by_email()).count() > 0:
            raise ValidationError(u'Пользователь с таким логином уже зарегистрирован! Укажите другую почту')
        try:
            hostname = value.split('@')[-1]
        except KeyError:
            raise ValidationError(u'Введите E-Mail')

        try:
            for server in [str(r.exchange).rstrip('.') \
                           for r \
                           in dns.resolver.query(hostname, 'MX')]:
                try:
                    smtp = smtplib.SMTP()
                    smtp.connect(server)
                    status = smtp.helo()
                    if status[0] != 250:
                        continue
                    smtp.mail('')
                    status = smtp.rcpt(value)
                    if status[0] != 250:
                        raise ValidationError(u'Такой почты не существует')
                    break
                except smtplib.SMTPServerDisconnected:
                    break
                except smtplib.SMTPConnectError:
                    continue
        except dns.resolver.NXDOMAIN:
            raise ValidationError(u'Такой почты не существует')
        except dns.resolver.NoAnswer:
            raise ValidationError(u'Такой почты не существует')
        return value