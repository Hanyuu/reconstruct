# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.users.register.views import RegistrationView

urlpatterns = patterns('',
                       url(r'register/(?P<parent_id>\d+)$', RegistrationView.as_view(), name='register')
)