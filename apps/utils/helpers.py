# -*- coding: utf-8  -*-
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import available_attrs
from functools import wraps
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect

def cbv_decorator(decorator):
    """
    Turns a normal view decorator into a class-based-view decorator.

    Usage:

    @cbv_decorator(login_required)
    class MyClassBasedView(View):
        pass
    """

    def _decorator(cls):
        cls.dispatch = method_decorator(decorator)(cls.dispatch)
        return cls

    return _decorator


def wizard_decorator(decorator):
    """
    Turns a normal :class:`FormWizard` into a decorator

    Usage:

    @wizard_decorator(login_required)
    class MyFormWizard(FormWizard):
        pass
    """

    def _decorator(cls):
        cls.__call__ = method_decorator(decorator)(cls.__call__)
        return cls

    return _decorator

def has_product(product_alias, redirect_url):
    def decorator(func):
        def inner_decorator(request, *args, **kwargs):
                user = request.user
                if user.products.all().filter(product__alias=product_alias).count() > 0:
                    return func(request, *args, **kwargs)
                else:
                    return HttpResponseRedirect(reverse(redirect_url))

        return wraps(func)(inner_decorator)

    return decorator

