import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cons.settings")

from django.contrib.auth.models import User
from apps.users.profiles.models import Profile
from django.core import management

# Drop the DB and recreate it
os.system('del /s /f test.sqlite')


# Run the syncdb
management.call_command('syncdb', interactive=False)
management.call_command('migrate', interactive=False)

# Create the super user and sets his password.
user = User.objects.create_superuser('hanyuu', 'x@x.com', '02006')
profile = Profile(user=user, parent=None)
profile.save()
os.system('manage.py runserver 0.0.0.0:80')