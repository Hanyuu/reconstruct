/**
*   @author Hanyuu
*   Where are all content elements & functions to control elements positions.
*/

var Geometry={
    /**
     * Where all content elements<br/>
     * [id:object]
     */
    contentElements:{},
    /**
     * Will serialize all content elements to JSON.<br/>
     * contentElements->JSON
     */
    toJSON:function(){
        var els=[];
        for(var i in this.contentElements){
            var el=this.contentElements[i];
            var childrens=[];
            for(var j in el.childrens){
                childrens.push(j);
            }
            els.push({
                id:i,
                left:el.JQueryEL.css('left'),  
                top:el.JQueryEL.css('top'),
                height:el.JQueryEL.css('height'),
                width:el.JQueryEL.css('width'),
                html:escape(el.JQueryEL.find('#wrap-'+el.id).html()),
                childrens:childrens,
                resizable:el.el.resizable,
                aspectRatio:el.el.aspectRatio,
                actions:el.el.actions,
                unpack:el.el.unpack,
                fromNew:true
            });
        }  
        els.push({
            id:'content-field',
            'color':$('.panel-content').css('color'),
            'background-color':$('.panel-content').css('background-color'),
            'width':$('.panel-content').css('width'),
            'height':$('.panel-content').css('height'),
            'background-image':$('.panel-content').css('background-image'),
            'keywords':escape($('.panel-content').attr('data-keywords')),
            'data-trap':escape($('.panel-content').attr('data-trap')),
            'data-trap-active':$('.panel-content').attr('data-trap-active'),
            'data-favicon':escape($('.panel-content').attr('data-favicon')),
            'data-title':escape($('.panel-content').attr('data-title')),
            'border':$('.panel-content').css('border-bottom-style')+' '+$('.panel-content').css('border-bottom-width')+' '+$('.panel-content').css('border-bottom-color')
        })
        return $.toJSON(els);
    },
    /**
     * Will deserialize JSON data to content elements & added its to content field</br>
     * JSON->contentElements
     */
    fromJSON:function(data){
        
        //FIXME: EVAL IS BAAAAD! But $.parseJSON not work in IE.
        try{
            var elements=(eval(data));
        }catch(e){
            alert('JSON PARSE ERROR!\n'+e);
            return;
        }
        elements.map(function(el){
            if(el.id=='content-field'){
                $('.panel-content').css({
                    'color':el['color'],
                    'background-color':el['background-color'],
                    'width':el['width'],
                    'height':el['height'],
                    'border':el['border'],
                    'background-image':el['background-image'],
                    
                    'background-size':' 100% 100%'
                })
                .attr('data-keywords',el['keywords']?unescape(el['keywords']):'')
                .attr('data-trap-active',el['data-trap-active']?el['data-trap-active']:'false')
                .attr('data-favicon',unescape(el['data-favicon']))
                .attr('data-title',unescape(el['data-title']))
                .attr('data-trap',el['data-trap']?unescape(el['data-trap']):'');
                
                $('.setka').css({
                    'width':el['width'],
                    'height':el['height']
                });
                var width_count = Math.ceil(parseInt(el['width'])/100);
                var height_count = Math.ceil(parseInt(el['height'])/100);
                var i =0;
                var width_count_str = '';
                while(i<=width_count){
                    i++;
                    width_count_str+='<span>'+i*100+'</span>';
                }

                $('.panel-content').find('.line_wight .body').html(width_count_str);
                
                
                
                var height_count_str = '';
                i=0;
                while(i<=height_count){
                    i++;
                    height_count_str+='<div><span>'+i*100+'</span></div>';
                }
                
                $('.panel-content').find('.line_height .body').html(height_count_str);
                //                h_body.innerHTML(height_count_str);
                
                
                
                
                $('.panel-content').children('.line_height').css({
                    'height':el['height']
                });
                $('.panel-content').children('.line_wight').css({
                    'width':el['width']
                });
            }else{
                var contentEl=new ElAppended(
                    el,
                    {
                        pageX:parseInt(el.left),
                        pageY:parseInt(el.top)
                    },true);
                contentEl.JQueryEL.css({
                    width:el.width,
                    height:el.height
                })
                Geometry.contentElements[el.id]=contentEl;
            }
        });

        for(var о in Geometry.contentElements){
            var _el=Geometry.contentElements[о];
            _el.childrensIds.map(function(id){
                _el.appendChildren(Geometry.contentElements[id]);
            });
            delete _el.childrensIds;
        }
    },
    /**
     *  @param e - mouse Event. (Object with fields pageX and pageY)<br/>
     *  @return Array of elements 
     */
    currentMousePosition:function(e){
        var els=[];
        for(var i in this.contentElements){
            var storedEl=this.contentElements[i];

            if(this.containsCords(e,storedEl.JQueryEL)){
                els.push(storedEl);
            }
        }
        return els;
    },
    containsCords:function(e,el){

        var offset=el.offset(),
        position=el.offset();
        return( 
            e.pageX>offset.left&&
                
            e.pageX<el.outerWidth(true)+position.left&&
            
            e.pageY>offset.top&&
            
            e.pageY<el.outerHeight(true)+position.top
            );
    },
    isElementsContains:function(el1,el2){

        var 
        l_x=el1.offset().left
        ,r_x=l_x+el1.outerWidth(true)
        
        ,l_y=el1.offset().top
        ,r_y=l_y+el1.outerHeight(true);
        /**
         *  @FIX: +/- 15 is cluge, which compensates "paddig:15px" of JQuery helper object
         *  Becous we can move Helper-Object (Not Content-Object!) outward panel-content
         */
        return(
            //L_X L_Y is Left Top angle   
            this.containsCords({
                pageX:l_x+15,
                pageY:l_y+15
            }, el2)&&
            //R_X R_Y is Right bottom angle         
            this.containsCords({
                pageX:r_x-15,
                pageY:r_y-15
            }, el2)
            );
        
    }
};
