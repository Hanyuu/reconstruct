# -*- coding: utf-8 -*-
from decimal import Decimal

from django.db import models
from django.contrib.auth.models import User
from django.db.models import Sum
from mptt.models import MPTTModel, TreeForeignKey


ORDER_STATUS = (
    (0, u'Выставлен'),
    (1, u'Отменён'),
    (2, u'Оплачен'),
)

class Category(MPTTModel):
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    name = models.CharField(u'Название',max_length=255)

class UserCategory(Category):
    owner = models.ForeignKey(User, related_name='user_categorys')

class Product(models.Model):
    price = models.DecimalField(u'Цена', max_digits=8, decimal_places=2, default=Decimal('0.0'), )
    name = models.CharField(u'Название', max_length=255, primary_key=True, unique=True)
    alias = models.CharField(u'Внутреннее имя', max_length=255, unique=True)
    category = models.ManyToManyField(Category, related_name='categorys')

    def __unicode__(self):
        return self.name

class UserSellProduct(Product):
    owner = models.ForeignKey(User, related_name='user_sell_products')
    user_category = models.ManyToManyField(UserCategory, related_name='user_categorys')


class Program(Product):
    pass


class Order(models.Model):
    product = models.ManyToManyField(Product, related_name='product')
    user = models.ForeignKey(User, related_name='user_orders')
    status = models.SmallIntegerField(u'Статус', choices=ORDER_STATUS, default=0)

    def __unicode__(self):
        return u'Заказ №%s'%self.pk

    @property
    def price(self):
        return self.product.all().aggregate(Sum('price'))['price__sum']

    def pay(self):
        #WHERE PAYMENTS
        #
        ##################
        for product in self.product.all():
            up = UserProduct(product=product, user=self.user)
            up.save()

        profile = self.user.get_profile()
        profile.role = 1
        profile.save()

        self.status = 2
        self.save()


class UserProduct(models.Model):
    product = models.OneToOneField(Product, related_name='user_product', primary_key=True)
    user = models.ForeignKey(User, related_name='products')
    date_payed = models.DateTimeField(u'Время покупки',auto_now=True)

    def __unicode__(self):
        return u'%s %s'% (self.product.name, self.user)



