/**
 * @author Hanyuu
 * 
 * Prototypes of HTML elements.
 * html:raw html code of el
 * actions:will show in menu, {clazz:css class,name:point to funcion in Prototypes.MenuFunctions}
 * (optional) unpack: point to unpack function in Prototypes.Unpacks 
 * (optional) resizable: is resizable?
 * (optional) onlyOne:only one element of such type can added in panel-content
 */
Prototypes.ContentElements={
    /**
     * YouTube
     */
    'el-youtube':{
        html:'<img style="width:420px; height:315px;" src="http://i.ytimg.com/vi/rMSdhgP08gU/0.jpg"></img>',
        actions:[{
            clazz:'edit',
            name:'editYoutube'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        unpack:'unpackYouTube',
        resizable:true,
        aspectRatio:true
    },
    /**
     * Text
     */
    'el-text':{
        html:'<div class="el_text" style="line-height:inherit; backgroud-color:transparent;float:left; width:351px;margin:0px;text-align:left;">Text</div>',
        actions:[{
            clazz:'edit',
            name:'editText'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true
    },
    /**
     * Img
     */
    'el-img':{
        html:'<img style="width:40px;height:40px;" src="http://demo.appsruel.com/static/img/logotype1.png"/>',
        actions:[{
            clazz:'edit',
            name:'editImage'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true,
        aspectRatio:true,
        unpack:'unpackIMG'
    },
    /**
     * Box
     */
    'el-div':{
        html:'<div style="width:100px;height:100px;"></div>',
        actions:[{
            clazz:'edit',
            name:'areaEdit'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true
    },
    /*
     * Link
     */
    'el-href':{
        html:'<a style="text-decoration:underline" href="http://www.ruelsoft.com/">RuEl Soft</a>',
        actions:[{
            clazz:'edit',
            name:'editLink'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:false
    },
    /**
     * Subscribe form
     */
    'el-form':{
        html:
        '<div style="float:left;"><style>'+
            'div.form{padding:10px;}div.form table{border-collapse: separate;} div.form,div.form div.button{float:left}div.form{font-family:Tahoma,"sans-serif"}div.form td.text{padding-right:10px;color:#343434}div.form td.text,div.form div.button a{font-size:14px}div.form input,div.form div.button{height:25px}div.form input{margin:0;padding:0 5px;border:solid 1px #7e7e7e;width:150px}div.form div.button{cursor:pointer;border-radius:4px;overflow:hidden;margin-top:4px;border-style:solid;border-width:1px}div.form div.button span{display:block;padding:3px 10px;height:100%;background:url("http://capture-sys.ruelsoft.org/img/form/button_bg.png") repeat-x 0 -2px}div.form table{border-spacing: 0 5px;}'+
       ' </style><div class="form">'+
                '<table class="table">'+
                    '<tbody>'+
                        '<tr id="FirstName">'+
                            '<td><input placeholder="Имя" type="text"/></td>'+
                        '</tr>'+
                        '<tr id="eMail">'+
                            '<td><input placeholder="E-Mail" type="text"/></td>'+
                        '</tr>'+
                    '</tbody>'+
                '</table>'+
                '<div style="border-color:#fc145b;background: #fc145b; color:#fff" class="button"><span>Подписаться</span></div>'+
        '</div></div>',
        actions:[{
            clazz:'edit',
            name:'editForm'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:false
    },
    /**
     * Raw HTML input
     */
    'el-html':{
        html:'<div style="width:40px;height:40px"></div>',
        actions:[{
            clazz:'edit',
            name:'editHtml'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true,
        unpack:'unpackHTML',
        aspectRatio:true
    },
    /**
     * Audio
     */
    'el-audio':{
        html:'<div id="audio" style="width:200px;height:20px" data-audio="http://capture-sys.ruelsoft.org/sound.mp3"><img src="/static/img/elements/audio-view.png" /></div>',
        actions:[{
            clazz:'edit',
            name:'editAudio'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        unpack:'unpackAudio',
        resizable:false,
        onlyOne:true
    },
    'el-visit-counter':{
        html:'<div id="visit-counter" class="counter_block">'+
            '<div>'+
                '<span>0</span>'+
            '</div>'+
        '</div>',
        actions:[{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:false,
        onlyOne:true
    }
};
