
/**
 *  @author Hanyuu
 *  EL on tools panel.
 *  @param type - type of element. </br>
 *  Handler for given type, must be defened in ElAppended constructor.</br>
 *  @param icon - link to image in "img/" which will be shown on tools panel.
 *
 */
var ElPanel=new function(){

    function ElPanel(type,icon,toolTip){
        var self=this;
        this.type=type;
        
        this.icon=icon;
        
        this.clearHTML='<div id="'+this.type+'" class="element-pic" data-tool-tip="'+toolTip+'"></div>';
        
        this.JQueryEL=$(this.clearHTML);
        
        this.JQueryEL.css('background','url(/static/img/elements/'+this.icon+') no-repeat');
        this.mouseOffset={
            x:0,
            y:0
        };
        var lastPanelElement=$('.drop').children('div').last();
        //If on tools panel is other instruments, move this instrument-EL to left
        if(lastPanelElement){
            var left=parseInt(lastPanelElement.css('left'));
            if(isNaN(left))
                this.JQueryEL.css('left',30) //IE fix
            else
                this.JQueryEL.css('left',left+50) 
        }
        
        this.JQueryEL.appendTo($('.drop'));

        this.JQueryEL.bind('mousedown',function(e){
            self.mouseOffset={
                x:e.pageX-self.JQueryEL.offset().left,
                y:e.pageY-self.JQueryEL.offset().top
            };
            EventEmitter.getInstance().emit('elSelected', {
                event:e,
                el:self
            });
        });
  
    }

    /**
     * Called on "mouse up". Will create ElAppended ib panel-content.
     */
    ElPanel.prototype.onDrop=function(e,gui){
        return new ElAppended(Prototypes.ContentElements[this.type],e);
    };
    ElPanel.prototype.ObjectType='ElPanel';
    return ElPanel;
}
/**
 *  Element in content panel.
 *  @param prototype : element from Prototypes.ContentElements<br/>
 *  @param e : mouse event<br/>
 *  @param fromTmpl : is loaded from template?
 */
var ElAppended=new function(){
    function ElAppended(prototype,e,fromTmpl){
        var self=this;
        this.menu=null;
        this.el=prototype;
        this.childrens={};
        this.startCords={};
        this.parent=undefined;
        this.id=prototype.id?prototype.id:new Date().getTime();
        this.allowMoving=true;
        if(prototype.childrens){
            this.childrensIds=prototype.childrens;
        }
        if(this.el){
            
            /**
            * Realy displayed HTML object. (youtube, img, etc)
            */
            if(fromTmpl)
                this.JQueryContent=$(unescape(this.el.html));
            else
                this.JQueryContent=$(this.el.html);
            
            if(this.el.onlyOne){
                var id=this.JQueryContent.attr('id');
                if($('.panel-content').find('#'+id).attr('id'))
                    return;
            }
            /**
            * Wrapper on realy displayed object
            */
            this.JQueryEL=$('<div><div class="wrap-el" id="wrap-'+this.id+'"></div></div>');
            
            
            this.JQueryEL.attr('id',this.id);

            this.JQueryContent.appendTo(this.JQueryEL.find('#wrap-'+this.id));
            
            this.JQueryEL.appendTo($('.panel-content'));
            
            var panelCords={
                left:0,
                top:0
            };
            if(!fromTmpl||(fromTmpl&&!this.el.fromNew))
                panelCords=$('.panel-content').offset();

            this.JQueryEL.css({
                position:'absolute',
                left:(e.pageX-panelCords.left)+'px',
                top:(e.pageY-panelCords.top)+'px',
                padding:'15px',
                'z-index':100
            }).css({
                border:'dotted 1px #656565'
            });
            this.mouseOffset={
                x:0,
                y:0
            };
            /**
             *  Create menu for edit this element
             */
            this.menu=$('<div class="menu-edit"></div>');
            /**
             *  Add actions to menu
             */
            this.el.actions.map(function(x){
                if(x){
                    var action=Prototypes.MenuFunctions[x.name];
                    if(!action)
                        throw 'Unknown action:'+x.name;
                    var actionLink=$('<a></a>');
                    actionLink.bind('click',function(){
                        self.changeZIndex(-100);
                        self.menu.hide(0);
                        action.call(self);
                        return false;
                    });
                    actionLink.addClass(x.clazz);
                    actionLink.appendTo(self.menu);
                }
            })
            self.menu.appendTo($(document.body)).hide();

            this.JQueryEL.bind('mousedown',function(e){
                self.mouseDownHandler(e);  
                return false;
            });
            
            this.JQueryEL.bind('keypress',function(e){
                return false;
            });
            if(!fromTmpl){
                EventEmitter.getInstance().emit('contentAdded', {
                    e:e,
                    el:this
                });
            }
            //THIS IS CLUGE! Written in 17.08.2012, when project is done, but element_layer not planned in febriary, when project written.
            var action=this.el.actions.length>1?(this.el.actions[0]==null?this.el.actions[1].name:this.el.actions[0].name):'editvisit-counter'; //Counter han no editor
            var name='';
            
            action=action.replace('edit','el-').toLowerCase();
            //We must know, what element is our element. 
            //Because we don't save to DB element type, we can understand type only by edit action.
            switch(action){
                case 'el-youtube':
                    name='Видео';
                    break;
                case 'el-text':
                    name='Текст';
                    break;
                case 'el-visit-counter':
                    name='Счётчик посещений';
                    break;
                case 'el-image':
                    name='Изображение';
                    break;
                case 'areaedit':
                    name='Контейнер';
                    break;
                case 'el-link':
                    name='Ссылка';
                    break;
                case 'el-form':
                    name='Форма подписки';
                    break;
                case 'el-html':
                    name='HTML код';
                    break;
                case 'el-audio':
                    name='Звук'
                    break;
            }
            var el=$('#additional-panel-el').tmpl({
                isGlobal:true,
                elClazz:action,
                elName:name,
                id:self.id
            });
            //Append El to work panel
            el.appendTo('.elem_body');
            
            var originalInputsWH={
                w:parseInt(self.JQueryContent.find('input').css('width')),
                h:parseInt(self.JQueryContent.find('input').css('height')),
                d_w:parseInt(self.JQueryEL.css('width'))/parseInt(self.JQueryContent.find('input').css('width')),
                d_h:parseInt(self.JQueryEL.css('height'))/parseInt(self.JQueryContent.find('input').css('height'))
            };
            if(this.el.resizable){
                this.JQueryEL.resizable({
                    handles: 'all',
                    aspectRatio:(this.el.aspectRatio?true:false),
                    resize: function(e,ui){
                        if(!self.JQueryContent.hasClass('el_text')){
                            self.JQueryContent.css('width',(parseInt(self.JQueryEL.css('width'))-5)+'px');
                            self.JQueryContent.css('height',(parseInt(self.JQueryEL.css('height'))-5)+'px');
                        }
                    },
                    start: function(event, ui) { 
                        EventEmitter.getInstance().emit('contentChanged');
                        self.allowMoving=false;
                        
                    },
                    stop:function(event, ui){
                        self.allowMoving=true;
                        /*self.JQueryContent.find('input').each(function(){
                            var el=$(this);
                            var w=(ui.size.width*originalInputsWH.d_w)/originalInputsWH.w
                                ,h=(ui.size.height*originalInputsWH.d_h)/originalInputsWH.h;
                                console.log(w,h)
                            el.css('width',((parseInt(el.css('width')))*w)+'px');

                            el.css('height',((parseInt(el.css('height')))*h)+'px');

                        });*/
                    }
                }).ruelResize();
            }
            //Will resize element by click on right panel element
            el.find('span').bind('click',function(e){
                e.preventDefault?e.preventDefault():e.returnValue = false;
                e.stopPropagation()
                var target=$(e.target).parent();
                $('.elem_content').find('.elem_li_active').removeClass('elem_li_active');
                target.find('.text-wrapper').addClass('elem_li_active');
                $('.panel-content').find('.selected-el').removeClass('selected-el');
                self.JQueryEL.addClass('selected-el');
                $(document).unbind('keydown').bind('keydown',function(e){
                    return GUI.getInstance().resizer(self, e);
                });
                
            });
            el.fb('.elem_edit','click',function(){
                if(self.el.actions.length==2)
                    Prototypes.MenuFunctions[self.el.actions[0].name].call(self);
            });
            el.fb('.elem_delete','click',function(){
                GUI.getInstance().showQuestion('Вы действительно хотите удалить элемент?', function(){
                   self.deleteEl(); 
                });
            });
        }else{
            throw 'Unknown Element type: '+prototype;
        }
    }
    /**
     *  Show edit menu
     */
    ElAppended.prototype.showMenu=function(){
        if($(document).find('.wrap').length==0){
            this.JQueryEL.css({
                'box-shadow':'' 
            }); 
            this.menu.css({
                position:'absolute',
                'z-index':this.JQueryEL.css('z-index')+1,
                left:this.JQueryEL.offset().left+parseInt(this.JQueryEL.css('width'))-7,
                top:this.JQueryEL.offset().top
            });
            this.menu.show(0);
        }
        return false;
    }
    /**
     * Will move element to last cords.
     */
    ElAppended.prototype.moveBack=function(){
        this.move(this.startCords.x,this.startCords.y);
    };
    /**
    *  Handler for "mouse up" event, when event clicked or stop moving
    */
    ElAppended.prototype.mouseUpHandler=function(e,gui){
        this.changeZIndex(-100);
        
        this.JQueryEL.css({
            'box-shadow':'' 
        });
        if(!Geometry.isElementsContains(this.JQueryEL,$('.panel-content')))
            this.moveBack();
        var els=Geometry.currentMousePosition(e)
        ,el=undefined;
        for(var i=0;i<els.length;i++){
            if(this.id!=els[i].id)
                el=els[i];
        }
        //In point, where we drop element is other item?
        if(el){
            //Finded item is it self?
            if(el.JQueryEL.attr('id')==this.JQueryEL.attr('id')&&el.parent){
                el.parent.removeChildren(el,gui);

            }
            //Finded item is olredy parent of selected item?
            if(this.parent&&
                el.JQueryEL.attr('id')==this.parent.JQueryEL.attr('id')){
                el.appendChildren(this); //Just store new cords of child
                gui.lastEl=el;
            }else if(el.JQueryEL.attr('id')!=this.JQueryEL.attr('id')){
                el.appendChildren(this,gui);
            }
        }else{
            //This is BUG. This must newer happen. Possibly currentMousePosition(e) need fix.
            if(this.parent){
                this.parent.removeChildren(this,gui);
            }
        }
        
    };
    /**
     *  Handler for "mouse down" event, user want to select element.
     */
    ElAppended.prototype.mouseDownHandler=function(e){
        EventEmitter.getInstance().emit('contentChanged');
        var self=this;
        $('.panel-content').find('.selected-el').removeClass('selected-el');
        this.JQueryEL.addClass('selected-el');
        $('.menu-edit').hide();
        self.changeZIndex(+100);
        self.startCords={
            x:self.JQueryEL.position().left,
            y:self.JQueryEL.position().top
        };
        
        self.mouseOffset={
            x:e.pageX-self.startCords.x,
            y:e.pageY-self.startCords.y
        };
        self.JQueryEL.css({
            'box-shadow':'0 0 10px rgba(0,0,0,0.5)' 
        });
        EventEmitter.getInstance().emit('elSelected', {
            event:e,
            el:self
        });
    };
    /**
     *  Remove onse children
     *  @param el : child to remove
     *  @param gui : point to GUI instance or NULL
     */
    ElAppended.prototype.removeChildren=function(el,gui){
        var self=this;
        var delChildren=function(){
            EventEmitter.getInstance().emit('contentChanged');
            el.parent=undefined;
            delete self.childrens[el.JQueryEL.attr('id')]; 
            el.changeZIndex(-100);
            var child=$('.elem_content').find('#panel-'+el.id)
            ,div=$('<div class="elem_item"></div>')
            ,li=child.parent('li');
            div.appendTo($('.elem_body'));
            child.appendTo(div);
            li.remove();

        };
        if(gui){
            gui.showQuestion('Вы действительно хотите разъединить элементы?',  
                function(){
                    delChildren();
                },
                function(){
                    el.JQueryEL.css({
                        left:el.startCords.x,
                        top:el.startCords.y
                        
                    }); 
                    EventEmitter.getInstance().emit('contentChanged');                      
                });
        }else{
            delChildren();
        }
         
    }
    /**
     *  Change Z-index for el & all childs
     */
    ElAppended.prototype.changeZIndex=function(count){
        var index=parseInt(this.JQueryEL.css('z-index'))+(count);
        if(index<100)
            return;
        this.JQueryEL.css({
            'z-index':index
        });
        for(var i in this.childrens){
            var child=this.childrens[i];
            child.changeZIndex(count);
        }
        
    }
    /**
     *  Add children to element
     */
    ElAppended.prototype.appendChildren=function(el,gui){
        if(!el)
            return;
        var self=this,
        storeCords=function(){
            //If we try to add parent to chiel as parent
            //Cross relations must not be where
            if(self.id in el.childrens){
            //No actions where?
            }else{
                EventEmitter.getInstance().emit('contentChanged');
                el.d_x=el.JQueryEL.position().left-self.JQueryEL.position().left;
                el.d_y=el.JQueryEL.position().top-self.JQueryEL.position().top;
                el.parent=self;
                self.childrens[el.JQueryEL.attr('id')]=el;
                el.changeZIndex(+100);
                var child=$('.elem_content').find('#panel-'+el.id)
                ,div=child.parent('div')
                ,parent=$('.elem_content').find('#panel-'+self.id)
                ,li=$('<li></li>').appendTo(parent);
                child.appendTo(li);
                div.remove();
            }
            
        };
        if(el.parent!=null)
            el.parent.removeChildren(el);
        if(gui){
            gui.showQuestion('Вы действительно хотите соеденить элементы?',  
                function(){
                    storeCords();   
                },
                function(){
                    EventEmitter.getInstance().emit('contentChanged');
                    el.JQueryEL.css({
                        left:el.startCords.x,
                        top:el.startCords.y
                    });
                    
                });
        }else{
            storeCords();
        }
        
    };
    /**
     *  Return cords {x,y}
     */
    ElAppended.prototype.getCords=function(){
        return {
            x:parseInt(this.JQueryEL.css('left')),
            y:parseInt(this.JQueryEL.css('top'))
        };  
    };
    /**
     *  Move elemet
     */
    ElAppended.prototype.move=function(x,y){
        if(this.allowMoving){
            this.JQueryEL.css({
                position:'absolute',
                left:(x)+'px',
                top:(y)+'px'
            });
            this.moveChildrens(x, y);
        }
    };
    /**
     *  Move element childs
     */
    ElAppended.prototype.moveChildrens=function(x,y){
        for(var i in this.childrens){
            var child=this.childrens[i];
            child.move(x+child.d_x,y+child.d_y);
        }
        
    };
    /**
     *  Del element
     */
    ElAppended.prototype.deleteEl=function(){
        
        EventEmitter.getInstance().emit('contentDeleted', this);
        EventEmitter.getInstance().emit('contentChanged');
        
        var el=$('.elem_content').find('#panel-'+this.id).parent();
        el.remove();
        var childrens=this.childrens;
        for(var i in childrens){
            childrens[i].deleteEl();
        }
        this.JQueryEL.remove();
        this.menu.remove();
        
    };   
    ElAppended.prototype.ObjectType='ElAppended';
    return ElAppended;
}
/**
 * "Abstract"
 * @see elementsFunctions.js
 * @see elementsPrototypes.js
 */
var Prototypes=new function(){
    function Prototypes(){}  
    return Prototypes;
}