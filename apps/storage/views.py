from django.shortcuts import render_to_response
from django.views.generic import View
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from apps.utils.helpers import cbv_decorator, has_product


@cbv_decorator(login_required(login_url = 'home'))
@cbv_decorator(has_product(product_alias='construct',redirect_url='office'))
class StorageView(View):
    def get(self, request):
        return render_to_response('storage.html', {}, RequestContext(request))
