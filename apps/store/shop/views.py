# -*- coding: utf-8 -*-
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from apps.utils.helpers import cbv_decorator
from django.shortcuts import render_to_response, redirect, resolve_url
from django.template import RequestContext
from apps.store.shop.models import Product,Order
from apps.payments.payeer.forms import PayeerForm
from apps.payments.payeer.utils import generate_data


@cbv_decorator(login_required)
class ProductBayView(View):
    def get(self, request, product_alias):
        try:
            product = Product.objects.get(alias=product_alias)
            order = Order()
            order.user = request.user
            order.save()
            order.product.add(product)

            form = PayeerForm(generate_data(order))
            return render_to_response('bay_product.html',{'form': form} ,RequestContext(request))
        except Exception,e:
            print e
            return render_to_response('product_not_exists.html', RequestContext(request))


    def post(self, request, product_id):
        pass