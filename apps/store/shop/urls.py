# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.store.shop.views import ProductBayView

urlpatterns = patterns('',
                       url(r'^shop/bay/(?P<product_alias>\w+)$', ProductBayView.as_view(), name='bay_product'),
)