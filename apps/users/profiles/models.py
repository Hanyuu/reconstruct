# -*- coding: utf-8 -*-

# Create your models here.
from mptt.models import MPTTModel, TreeForeignKey
from django.db import models
from django.contrib.auth.models import User

ROLE_USERS = (
    (0, u'Пользователь'),
    (1, u'Оплативший заказ пользователь'),
)


class Profile(MPTTModel):
    user = models.OneToOneField(User, primary_key=True, related_name='profile')

    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    role = models.SmallIntegerField(u'Роль', choices=ROLE_USERS, default=0)

    def __unicode__(self):
        return self.user.username