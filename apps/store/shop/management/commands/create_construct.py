# -*- coding: utf-8 -*-
from decimal import Decimal

from django.core.management.base import BaseCommand

from apps.store.shop.models import Product


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            product = Product(name=u'Конструктор сайтов многостраничников', price=Decimal('100.00'))
            product.save()
            print product.pk
        except Exception, e:
            print e
