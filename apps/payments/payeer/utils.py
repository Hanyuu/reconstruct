# -*- coding: utf-8 -*-
'''
<?
$m_shop = '4529511';
$m_orderid = '1';
$m_amount = number_format(100, 2, '.', '');
$m_curr = 'USD';
$m_desc = base64_encode('Test');
$m_key = 'secret_key';

$arHash = array(
	$m_shop,
	$m_orderid,
	$m_amount,
	$m_curr,
	$m_desc,
	$m_key
);
$sign = strtoupper(hash('sha256', implode(":", $arHash)));
?>
'''
import base64
import hashlib
from django.conf import settings
from apps.store.shop.models import Order

def generate_data(order):
    m_desc = base64.b64encode(order.__unicode__().encode('utf-8'))
    plain_sign = u'%s:%s:%s:%s:%s:%s'%(settings.PAYEER_SHOP_ID, order.pk, order.price ,u'USD', m_desc, settings.PAYEER_SHOP_KEY)
    sign = generate_sign(plain_sign)
    return {'m_shop': settings.PAYEER_SHOP_ID, 'm_orderid': order.pk, 'm_amount':order.price, 'm_curr':u'USD', 'm_desc': m_desc, 'm_sign':sign}

def generate_sign(data):
    return hashlib.sha256(data).hexdigest().upper()

def test():
    o = Order.objects.get(pk=1)
    print generate_data(o)

