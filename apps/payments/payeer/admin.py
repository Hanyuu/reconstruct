# -*- coding: utf-8 -*-
from django.contrib import admin

from apps.payments.payeer.models import PayeerResponse


class PayeerAdmin(admin.ModelAdmin):
    readonly_fields = ['operation_id', 'amount', 'operation_date', 'order', 'status',]
    list_filter = ('status',)

admin.site.register(PayeerResponse, PayeerAdmin)