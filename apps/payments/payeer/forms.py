# -*- coding: utf-8 -*-
'''
<form method="GET" action="//payeer.com/api/merchant/m.php">
<input type="hidden" name="m_shop" value="<?=$m_shop?>">
<input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
<input type="hidden" name="m_amount" value="<?=$m_amount?>">
<input type="hidden" name="m_curr" value="<?=$m_curr?>">
<input type="hidden" name="m_desc" value="<?=$m_desc?>">
<input type="hidden" name="m_sign" value="<?=$sign?>">
<input type="submit" name="m_process" value="send" />
</form>
'''
from django import forms

class PayeerForm(forms.Form):
    m_shop = forms.CharField(widget=forms.HiddenInput())
    m_orderid = forms.CharField(widget=forms.HiddenInput())
    m_amount = forms.CharField(widget=forms.HiddenInput())
    m_curr = forms.CharField(widget=forms.HiddenInput())
    m_desc = forms.CharField(widget=forms.HiddenInput())
    m_sign = forms.CharField(widget=forms.HiddenInput())
