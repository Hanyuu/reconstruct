# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.users.profiles.views import OfficeView, IndexView

urlpatterns = patterns('',
                       url(r'^$', IndexView.as_view(), name='home'),
                       url(r'office/$', OfficeView.as_view(), name='office')
)