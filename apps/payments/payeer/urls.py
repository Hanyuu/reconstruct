# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from apps.payments.payeer.views import FailedView, SuccessView ,StatusView
urlpatterns = patterns('',
                       url(r'^payments/payeer/success/$', SuccessView.as_view(), name='payer_success'),
                       url(r'^payments/payeer/fail/$', FailedView.as_view(), name='payer_fail'),
                       url(r'^payments/payeer/status/$', StatusView.as_view(), name='payer_status'),
)