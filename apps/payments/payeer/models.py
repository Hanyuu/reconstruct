# -*- coding: utf-8 -*-
from django.db import models
from decimal import Decimal

from apps.store.shop.models import Order

STATUS = (
    (0, u'Платёж выставлен'),
    (1, u'Успешный платёж'),
    (2, u'Не удачный платёж'),
)

class PayeerResponse(models.Model):
    operation_id = models.IntegerField()
    amount = models.DecimalField(max_digits=8, decimal_places=2, default=Decimal('0.0'), )
    status = models.SmallIntegerField(u'Статус', choices=STATUS, default=0)
    operation_date = models.DateTimeField()
    order = models.ForeignKey(Order, related_name='payeer_orders')

    def __unicode__(self):
        return u'Оплата заказа № %s'%self.order.pk