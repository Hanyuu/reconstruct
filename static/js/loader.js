var p=function(msg){
    try{
        console.log(msg)
    }catch(e){
        
    }
}

$(document).ready(function(){
    var gui=new GUI(),
    doc=$(this),
    world=new World(gui),
    loadCallback;
    //If we send form from iframe, we cant get reply. 
    //This function will called from server reply, and do any actions.
    window['iframe_callback']=function(data){
        if(!data){
            loadCallback();
        }else{
            switch(data.code){
                case 'failed':
                    break;
                case 'ok': //Reply Ok, parse type of reply.
                    switch(data.type){
                        case 'img-uploaded':
                            EventEmitter.getInstance().emit('img-uploaded', data);
                            break;
                        default:
                            throw 'Unknown data.type: '+data.type;
                    }
                    break;
                case 'loaded':
                    alert(data)
                default:
                    throw 'Unknown data.code: '+data.code;
            }
        }
    }
    
    //<A> tags prevent 
    $('a').live('click',function(){

        if(!$(this).hasClass('lk')&&!$(this).hasClass('exit')&&$(this).parent().attr('id')!='disk-name'&&!$(this).parent().is('li')){
            return false;
        }
    })
    EventEmitter.getInstance().on('elSelected', function(data){
        gui.onElSelected(data); 
    });
    
    var elYouTube=new ElPanel('el-youtube','video.png','Элемент видео. Для активации элемента перетащите его на рабочую область.');
    var elP=new ElPanel('el-text','text.png','Элемент текст. Для активации элемента перетащите его на рабочую область.');
    var elImg=new ElPanel('el-img','img.png','Элемент картинка. Для активации элемента перетащите его на рабочую область.');
    var visitCounter=new ElPanel('el-visit-counter','visits-counter.png','Элемент счётчик посещений. Для активации элемента перетащите его на рабочую область.');
    var elDiv=new ElPanel('el-div','box.png','Элемент контейнер. Для активации элемента перетащите его на рабочую область.');
    var elHref=new ElPanel('el-href','link.png','Элемент ссылка. Для активации элемента перетащите его на рабочую область.');
    //var elForm=new ElPanel('el-form','form.png','Элемент форма подписки. Для активации элемента перетащите его на рабочую область.');
    var elHtml=new ElPanel('el-html','html.png','Элемент для вставки HTML кода. Для активации элемента перетащите его на рабочую область.');
    //var elAnimatedLogo=new ElPanel('el-animated-logo','img.png','');
    var elAudio=new ElPanel('el-audio','audio.png','Элемент для вставки звука. Для активации элемента перетащите его на рабочую область.');
    //View HTML code or page preview
    $('.view, .html, .html_view').bind('click',function(e){
        var html,
        host=window.location.host,
        recipe =  window.open('/empty/','_blank');
        loadCallback=function(){
            recipe.document.write(html)
        }
        if(e.ctrlKey){
            html=Geometry.toJSON().replace(/</g,'&lt;').replace(/>/g,'&gt;');
        }else{
            html=gui.generateHTML(Geometry.contentElements,
                ($(this).hasClass('html')||
                    $(this).hasClass('html_view'))
                );     
            
            if($(this).hasClass('html')||$(this).hasClass('html_view'))
                html=html.replace(/&gt;/gi,'&gt;<br/>');
            
        }

        
    /*$(recipe.document).ready(function(){
            $(recipe.document).contents().html(html) 
        })*/
    });

    //Save page
    $('.save').bind('click',function(){
        if($('.page_name').text().indexOf('Безымянный проект')!=-1){
            gui.showOpenAndSaveDialog(true);
        }else{
            gui.saveCurrentProject();
        }
        
    });
    //Save page
    $('.saveas').bind('click',function(){
        gui.showOpenAndSaveDialog(true);
    });
    //Clear content field
    $('div.right a.clear').bind('click',function(){
        gui.clearField(true);  
    });
    //Template loading
    $('#templates').bind('change',function(){
        gui.loadTemplate();
    });
    //Save template
    $('.save_as').bind('click',function(){
        gui.saveTemplate();
    });
    //Template name edit
    $('#edit-tmpl-name').bind('click',function(){
        gui.editTemplateName()
    });
    //MouseUP handler
    doc.bind('mouseup',function(e){
        gui.onMouseUp(e);  
    });
    //Mouse move handler
    doc.bind('mousemove',function(e){
        gui.onMouseMove(e); 
    });
    //Click handler
    doc.bind('click',function(e){
        gui.onMouseClick(e);
    });
    //On mouse press - remove all tool-tips
    doc.bind('mousedown',function(){
        $('.tool-tip').remove();
    });
    //For edit CSS props of conten field;        
    $('div.right a.edit').bind('click',function(){
        var el={
            JQueryContent:$('.panel-content')
        }
        Prototypes.MenuFunctions.areaEdit.call(el,true);
    });
    //Show useflull links
    $('.help').bind('click',function(){
        gui.showHelp();
    });
    //Show all templates
    $('#all-templates-show').bind('click',function(){
        gui.showTemplates();
    });
    //Show all tpages
    $('#all-pages-show').bind('click',function(){
        gui.showPages();
    });
    //On/Off tool-tips
    $('#tool-tip-off').bind('click',function(){
        $.cookie('tool-tips-off', $(this).is(':checked'), {
            expires: 365, 
            path: '/'
        });
    });
    //Load tool-tips on/off from cookies
    if($.cookie('tool-tips-off')&&$.cookie('tool-tips-off')!='false'){
        $('#tool-tip-off').attr('checked','true')
    }
    //new template
    $('#create-new').bind('click',function(){
        gui.clearField(true);
    });
    //Show video-help
    var showVideo=function(checkBox){
        var form=$.tmpl($('#video-help-tmpl'),{
            checkBox:checkBox
        });
        form.fb('#dont-show-video-on-start','click',function(){
            $.cookie('dont-show-video-on-start', 'true', {
                expires: 365, 
                path: '/'
            });
            form.remove();
        });
        form.showDialog(); 
    };
    /*if(!$.cookie('dont-show-video-on-start')&&
        $.cookie('dont-show-video-on-start')!='false')
        showVideo(true);*/
    //Show video help by click in help
    $('#video-help').bind('click',function(){
        showVideo(false);
    });
    //For stupid users prevent unload window
    $(window).bind('beforeunload',function(){
        if(location.host.toString().indexOf('127.0.0.1')==-1
            &&
            location.host.toString().indexOf('localhost')==-1
            &&
            location.host.toString().indexOf('dev.ruelsoft')==-1
            &&
            location.host.toString().indexOf('.local')==-1)
            return 'Все не сохранённые данные будут потеряны.';
    });
    //Gradient line    
    $(window).bind('scroll',function(e){
        $('.line_wight').css({
            'left': -(window.scrollX)+$('.line_wight').parent().position().left+1
        });
    });   
    //Show&Hide menu
    $('.menu_but').bind('click',function(){
        var self=this;
        if(!$(this).parent().find('.menu_body .menu').is(':visible')){
            $('.menu_but').parent().find('.menu_body .menu').find('a').bind('click',function(){
                $(self).removeClass('select').parent().find('.menu_body .menu').slideUp();
            });
            $(this).addClass('select').parent().find('.menu_body .menu').slideDown().children().children('.close').unbind('click').bind('click',function(){
                $(self).removeClass('select').parent().find('.menu_body .menu').slideUp();
            });
        }else{
            $(this).removeClass('select').parent().find('.menu_body .menu').slideUp();
        //$('.menu_but').parent().find('.menu_body .menu').find('a').unbind('click');
        }
    });
    
    //CTRL+Z <-
    $('.back').bind('click',function(){
        world.rollBack();
    });
    //CTRL+Z ->
    $('.next').bind('click',function(){
        world.rollForward();
    });
    //Fix for lost sesson
    setInterval(function(){
        $.get('/ajax/regensessid');
    },600*1000);  

    //Show open diaolog
    $('.open').bind('click',function(){
        gui.showOpenAndSaveDialog(false);
    });
    //Admin adding tmpl
    $('#send').bind('click',function(){
        var data={};
        $('#add-form').find('input, textarea').each(function(){
            var el=$(this);
            data[el.attr('name')]=el.val();
        }); 
        $.post('/ajax/addtemplate/',data,function(data){
            alert(data); 
        });
    });
    //Publish project
    $('.public').bind('click',function(){
        gui.publishProject();
    });
    //Show link
    $('.link,.http').bind('click',function(){
        gui.showLink(); 
    });
    //Clear field
    $('.new, .create').bind('click',function(){
        gui.clearField(true);
    });
    //Show panel
    $('.panel_hide').bind('click',function(){
        $(this).toggleClass('panel_show') 
        $(this).parent().toggleClass('element_hide'); 
    });
    
});