# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'shop_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['shop.Category'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            (u'lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'shop', ['Category'])

        # Adding model 'UserCategory'
        db.create_table(u'shop_usercategory', (
            (u'category_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['shop.Category'], unique=True, primary_key=True)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user_categorys', to=orm['auth.User'])),
        ))
        db.send_create_signal(u'shop', ['UserCategory'])

        # Adding model 'Product'
        db.create_table(u'shop_product', (
            ('price', self.gf('django.db.models.fields.DecimalField')(default='0.0', max_digits=8, decimal_places=2)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, primary_key=True)),
            ('alias', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal(u'shop', ['Product'])

        # Adding M2M table for field category on 'Product'
        m2m_table_name = db.shorten_name(u'shop_product_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('product', models.ForeignKey(orm[u'shop.product'], null=False)),
            ('category', models.ForeignKey(orm[u'shop.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['product_id', 'category_id'])

        # Adding model 'UserSellProduct'
        db.create_table(u'shop_usersellproduct', (
            (u'product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['shop.Product'], unique=True, primary_key=True)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user_sell_products', to=orm['auth.User'])),
        ))
        db.send_create_signal(u'shop', ['UserSellProduct'])

        # Adding M2M table for field user_category on 'UserSellProduct'
        m2m_table_name = db.shorten_name(u'shop_usersellproduct_user_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('usersellproduct', models.ForeignKey(orm[u'shop.usersellproduct'], null=False)),
            ('usercategory', models.ForeignKey(orm[u'shop.usercategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['usersellproduct_id', 'usercategory_id'])

        # Adding model 'Program'
        db.create_table(u'shop_program', (
            (u'product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['shop.Product'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'shop', ['Program'])

        # Adding model 'Order'
        db.create_table(u'shop_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user_orders', to=orm['auth.User'])),
            ('status', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'shop', ['Order'])

        # Adding M2M table for field product on 'Order'
        m2m_table_name = db.shorten_name(u'shop_order_product')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('order', models.ForeignKey(orm[u'shop.order'], null=False)),
            ('product', models.ForeignKey(orm[u'shop.product'], null=False))
        ))
        db.create_unique(m2m_table_name, ['order_id', 'product_id'])

        # Adding model 'UserProduct'
        db.create_table(u'shop_userproduct', (
            ('product', self.gf('django.db.models.fields.related.OneToOneField')(related_name='user_product', unique=True, primary_key=True, to=orm['shop.Product'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='products', to=orm['auth.User'])),
            ('date_payed', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'shop', ['UserProduct'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table(u'shop_category')

        # Deleting model 'UserCategory'
        db.delete_table(u'shop_usercategory')

        # Deleting model 'Product'
        db.delete_table(u'shop_product')

        # Removing M2M table for field category on 'Product'
        db.delete_table(db.shorten_name(u'shop_product_category'))

        # Deleting model 'UserSellProduct'
        db.delete_table(u'shop_usersellproduct')

        # Removing M2M table for field user_category on 'UserSellProduct'
        db.delete_table(db.shorten_name(u'shop_usersellproduct_user_category'))

        # Deleting model 'Program'
        db.delete_table(u'shop_program')

        # Deleting model 'Order'
        db.delete_table(u'shop_order')

        # Removing M2M table for field product on 'Order'
        db.delete_table(db.shorten_name(u'shop_order_product'))

        # Deleting model 'UserProduct'
        db.delete_table(u'shop_userproduct')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'shop.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['shop.Category']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'shop.order': {
            'Meta': {'object_name': 'Order'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'product'", 'symmetrical': 'False', 'to': u"orm['shop.Product']"}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_orders'", 'to': u"orm['auth.User']"})
        },
        u'shop.product': {
            'Meta': {'object_name': 'Product'},
            'alias': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'categorys'", 'symmetrical': 'False', 'to': u"orm['shop.Category']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '8', 'decimal_places': '2'})
        },
        u'shop.program': {
            'Meta': {'object_name': 'Program', '_ormbases': [u'shop.Product']},
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['shop.Product']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'shop.usercategory': {
            'Meta': {'object_name': 'UserCategory', '_ormbases': [u'shop.Category']},
            u'category_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['shop.Category']", 'unique': 'True', 'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_categorys'", 'to': u"orm['auth.User']"})
        },
        u'shop.userproduct': {
            'Meta': {'object_name': 'UserProduct'},
            'date_payed': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'product': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'user_product'", 'unique': 'True', 'primary_key': 'True', 'to': u"orm['shop.Product']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['auth.User']"})
        },
        u'shop.usersellproduct': {
            'Meta': {'object_name': 'UserSellProduct', '_ormbases': [u'shop.Product']},
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_sell_products'", 'to': u"orm['auth.User']"}),
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['shop.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'user_category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_categorys'", 'symmetrical': 'False', 'to': u"orm['shop.UserCategory']"})
        }
    }

    complete_apps = ['shop']