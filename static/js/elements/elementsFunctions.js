/**
*   @author Hanyuu
*   All functions for editing elements using in project, also function for "unpack" elements & JQuery Extensions here.
*
*/


//-----------------------------------------------------------------------------------
//      JQuery EXTENSIONS
//-----------------------------------------------------------------------------------

$.fn.extend({
    ruelResize:function(){
        var el=$(this);
        el.addClass('resize');
        $('<span class="top-left"></span>'+
            '<span class="top-right"></span>'+
            '<span class="bottom-right"></span>'+
            '<span class="bottom-left"></span>'+
            '<span class="middle-left"></span>'+
            '<span class="middle-right"></span>'+
            '<span class="center-top"></span>'+
            '<span class="center-bottom"></span>')
        .appendTo(el);
        return this;
    } 
    
});
$.fn.extend({
    appendBorderEditor:function(el,additionalEl){
        var form=$(this);
       p(form)
        form.fb('#btn-border-size','change',function(){
            form.attr('data-border-size',($(this).val()+'px')); 
            if(additionalEl){
                additionalEl.css('border',form.find('.buttons').children('.select').attr('data-border')+' '+form.attr('data-border-size'));
            }
        });
       
        form.find('#btn-border-size').val(parseInt(el.css('border-bottom-width')));
        form.attr('data-border-size',el.css('border-bottom-width'));
        switch(el.css('border-bottom-style').trim()){
            case 'dashed':
                form.find('.but1').addClass('select');

                break;
            case 'inset':
                form.find('.but2').addClass('select');

                break;
            case 'outset':
                form.find('.but3').addClass('select');

                break;
            case 'double':
                form.find('.but4').addClass('select');

                break;
            case 'none':
            case 'solid':
                form.find('.but5').addClass('select');
                form.find('#btn-border-size').val(0);
                break;
        }

        form.fb('.buttons','click',function(e){
            e=$(e.target);
            $(this).find('.select').removeClass('select');
            if(!e.hasClass('but5')){
                var bs=parseInt(form.find('#btn-border-size').val());
                if(bs==0){
                    form.find('#btn-border-size').val(4);
                    form.attr('data-border-size','4px');
                }else{
                    form.attr('data-border-size',bs+'px');
                    
                }
                p(form.attr('data-border-size'))
            }
            
            if(additionalEl){
                additionalEl.css('border',e.attr('data-border')+' '+form.attr('data-border-size'));
            }
            e.addClass('select');
        });
        if(additionalEl){
            additionalEl.css('border',el.css('border-bottom-style')+' '+parseInt(el.css('border-bottom-width')));
        }
    } 
});
/**
 * Will show all dialogs in project
 */
$.fn.extend({
    showDialog:function(params){
        params=params?params:{
            title:null,
            zIndex:null,
            beforCenter:null //Function will call before dialog will centered
        };
        var form=$(this);
        //Set title
        if(params.title)
            form.find('.d_title').find('span').text(params.title);
        //Close by close btn & close by red cross
        form.fb('#close-btn,.d_close','click',function(){
            form.remove();
        });

        form.appendTo('body');

        if(params.beforCenter)
            params.beforCenter()
               var dialog=form.find('.dialog').css('width')==undefined?form.find('.window_support'):form.find('.dialog')
     dialog.css({
            'left':'50%',
            'top':'50%',
            'margin-left':(parseInt(dialog.css('width'))/-2)+'px',
            'margin-top':(parseInt(dialog.css('height'))/-2)+'px',
            position:'fixed'
        });

    
        if(params.zIndex){
            form.find('.dialog').css({
                'z-index':params.zIndex
            });
            form.find('.wrap').css('z-index',params.zIndex-100);
        } 
        else{
            form.find('.dialog').css({
                'z-index':11000
            });
            form.find('.wrap').css('z-index',10000);
        }
        
        return form;
    }
});

$.fn.extend({
    /**
     *  Find & bind
     */
    fb:function(selector,action,callback){
        this.find(selector).bind(action,function(e){
            callback.call(this,e);
        });
        return this;
    } 
});
/**
 * Return color in HEX format
 */
$.fn.extend({
    getHexColor:function(color){
        var val=this.css(color);
        if(!val)
            return;
        var toHex=function (N) {
            if (N==null) return "00";
            N=parseInt(N);
            if (N==0 || isNaN(N)) return "00";
            N=Math.max(0,N);
            N=Math.min(N,255);
            N=Math.round(N);
            return "0123456789ABCDEF".charAt((N-N%16)/16)
            + "0123456789ABCDEF".charAt(N%16);
        } 
        if(val.indexOf('transparent')>-1){
            return 'transparent';
        }else if(val.indexOf('rgb(')>-1){
            var rgb=val.replace('rgb(','').replace(')','').replace(',','').split(' ');
            var r=rgb[0];
            var g=rgb[1];
            var b=rgb[2];
            return '#'+toHex(parseInt(r))+  
            toHex(parseInt(g))+
            toHex(parseInt(b));
        
        }else if(val.indexOf('#')>-1){
            return val;
        }
    } 
});
/**
 *  Convert from unix date to human date
 *  TODO: to russion & human format
 */
$.extend({
    parseDate:function(date){
        var time = new Date(date*1000);
        return time.toLocaleDateString()+' '+time.getHours()+':'+time.getMinutes()+':'+time.getSeconds(); 
    }
});
/**
 * Adding color picker to form
 */
$.fn.extend({
    appendColorPicker:function(el,additionalEl,form){
        var selectedInput=undefined,
        self=this;
        self.find('.for_col_pic').each(function(){
            var el=$(this);
            var classElem=el.find('label').attr('for');
            if(classElem){
                el.find('.tuker').addClass(classElem);
                var div_color=el.find('.opt > div.color_box').find('div.color');
                div_color.attr('id',classElem+'-'+div_color.attr('id'));
                div_color.attr('class',classElem+'-'+div_color.attr('class'));
                var input=el.find('input#color-edit');
                input.attr('id',classElem+'-'+input.attr('id')); 
               
            }
        });

        self.find('#text-color').css({
            'background-color':el.css('color')
        });
        
        self.find('#bg-color').css({
            'background-color':el.css('background-color')
        });
        self.find('#bg-form-color').css({
            'background-color':$(form).css('background-color')
        });
        
        self.find('#border-color').css({
            'background-color':el.css('border-bottom-color')
        });
        
        self.attr('data-border-color',el.getHexColor('border-bottom-color'));
        /**
        * Show colors menu by click
        */
        self.fb('.tuker','click',function(e){
            if(!$(e.target).hasClass('tuker'))
                return;
            var self=$(this);
            if(self.hasClass('select')){
                self.removeClass('select');
            }else{
                self.addClass('select'); 
                selectedInput=self.parent().find('#bg-color-edit,#text-borber-color-edit').first();
            }
        });

        /**
        * Set selected color by click
        */
        self.fb('.color, .no_color','click',function(){
            var parent=$(this).parent().parent().parent().parent().parent(),
            box=parent.children('.color_box').children('div'),
            
            input=parent.children('input'),
            el=$(this);
            input.val(el.getHexColor('background-color'));
            if(input.attr('id')=='border-color-edit')
                self.attr('data-border-color',el.getHexColor('background-color'));
            
            box.css('background-color',el.css('background-color'));
            
            if(el.hasClass('no_color')){
                input.val('transparent');
                box.css('background-color','transparent');  
            }
            
            if(additionalEl){
                var type=self.find('.buttons').children('.select').attr('data-border');
                additionalEl.css({
                    'background-color': self.find('#bg-color-edit').val(),
                    'color': self.find('#text-color-edit').val(),
                    'border':(type+' '+self.attr('data-border-size')+' '+self.find('#border-color-edit').val())
                });
            }
        });
        selectedInput=$(this.find('#bg-color-edit, #border-color-edit').bind('click',function(){
            selectedInput=$(this);
        })[0]);
        /** 
        * User can enter color in HEX by you self - TEXT
        */
        self.fb('#text-color-edit','keyup',function(){
            
            self.find('#text-color').css({
                'background-color':$(this).val()
            });  
        });
        /**
        * User can enter color in HEX by you self - FONT
        */
        self.fb('#bg-edit','keyup',function(){
            self.find('#bg-color').css({
                'background-color':$(this).val()
            }); 
        });
        /**
        * User can enter color in HEX by you self - BORDER
        */
        self.fb('#border-color-edit','keyup',function(){
            self.find('#border-color').css({
                'background-color':$(this).val()
            }); 
            self.attr('data-border-color',self.find('#border-color').getHexColor('background-color'));
        });
        /**
        * Show Color Picker by click
        */
        self.fb('.butts .other','click',function(){
            self.find('.main, .other, .butts').hide();
            $(document.body).find('.tuker').each(function(){
                $(this).removeClass('select');
            });
            self.find('#colors-selector').first().show().ColorPicker({
                flat:true,
                onChange:function(hsb, hex, rgb){
                    var type=self.find('.buttons').children('.select').attr('data-border');
                    if(selectedInput){
                        selectedInput.val('#'+hex);
                        $('#'+selectedInput.attr('id').replace('-edit','')).css('background-color','#'+hex);
                    }
                    self.attr('data-border-color',self.find('#border-color-edit').val());
                    if(additionalEl){
                        if(!additionalEl.execCommand){
                            additionalEl.css({
                                'background-color': self.find('#bg-color-edit').val(),
                                'color': self.find('#text-color-edit').val(),
                                'border':type+' '+self.attr('border-size')+' '+self.find('#border-color-edit').val()
                            });
                        }else{
                            additionalEl.execCommand('backColor',false,self.find('#bg-color-edit').val());
                            additionalEl.execCommand('backColor',false,self.find('#text-color-edit').val());
                        }
                    }
                }
            });
        });
        
        self.find('#bg-color-edit').val(el.getHexColor('background-color'));
        self.find('#text-color-edit').val(el.getHexColor('color'));
        self.find('#border-color-edit').val(el.getHexColor('border-bottom-color'));
        self.find('#bg-form-color-edit').val($(form).getHexColor('background-color'));
    } 
});

//-----------------------------------------------------------------------------------
//                       UNPACK FUNCTIONS
//-----------------------------------------------------------------------------------

/**
 * Functions to convert in REALLY html code. Just for FLASH elements.
 */
Prototypes.Unpacks={
    unpackIMG:function(pos){
        var img='<img  src="'+this.JQueryContent.attr('src')+'" style="position:absolute;margin-left:'+pos.left+';margin-top:'+pos.top+';z-index:'+pos.zIndex+';width:'+pos.width+'; height:'+pos.height+';"/>';
        if(this.JQueryContent.attr('link-active')){
            return '<a target="_blank" href="'+this.JQueryContent.attr('img-href')+'">'+img+'</a>'
        }else{
            return img;
        }
    },
    unpackYouTube:function(pos){
        
        if(this.JQueryContent.attr('data-ruel-link')){
            return '<object style="position:absolute;margin-left:'+pos.left+
            ';margin-top:'+pos.top+
            ';z-index:'+pos.zIndex+';" id="videoPlayer" width="'+pos.width+'" height="'+pos.height+'"'+
            
            ' style="visibility: visible;" data="http://storage.ruelsoft.org/storage/static/swf/uppod.swf" type="application/x-shockwave-flash">'+
            '<param value="videoPlayer" name="id">'+
            '<param value="true" name="allowFullScreen">'+
            '<param value="uid=videoPlayer&m=video&file='+this.JQueryContent.attr('data-ruel-link')+($(this.JQueryContent).attr('data-autoplay')?'&auto=play':'')+'" name="flashvars">'+
            '<param value="always" name="allowScriptAccess">'+
            '<param name="wmode" value="opaque" />'+
            '<param name="AllowNetworking" value="all"></object>';
        }else{
            return '<iframe width="'+pos.width+'" height="'+pos.height+'" src="http://www.youtube.com/embed/'
            +this.JQueryContent.attr('src').split('/')[4]+
            ''+($(this.JQueryContent).attr('data-autoplay')?'?autoplay=1':'')+'" style="position:absolute;margin-left:'+pos.left+
            ';margin-top:'+pos.top+';'+
            'z-index:'+pos.zIndex+';'+
            '"  frameborder="0"allowfullscreen></iframe>';
        }
    },
    unpackAudio:function(pos){
        var start=this.JQueryContent.attr('data-onstart')=='true'?'1':'0';
        return  '<div style="position:absolute;margin-left:'+pos.left+';margin-top:'+pos.top+
        ';z-index:'+pos.zIndex+';'+'">'+
        '<object data="http://capture-sys.ruelsoft.org/swf/player_mp3.swf" width="200" height="20">'+
        '<param name="movie" value="http://capture-sys.ruelsoft.org/swf/player_mp3.swf" />'+
        '<param name="FlashVars" value="mp3='+this.JQueryContent.attr('data-audio')+'&amp;autoplay='+start+'&amp;'+this.JQueryContent.attr('data-decoration')+'"/>'+
        
        '</object></div>';
    },
    unpackHTML:function(pos){
        if(!this.JQueryContent.children().attr('data-flash')){
            return '<div style="position:absolute;margin-left:'
            +pos.left+';margin-top:'
            +pos.top+';width:'+pos.width+
            '; height:'+pos.height+
            ';z-index:'+pos.zIndex+';">'+this.JQueryContent.html()+'</div>';
        }else{
            return  '<div style="position:absolute;margin-left:'
            +pos.left+';margin-top:'
            +pos.top+';width:'+pos.width+'; height:'+pos.height+
            ';z-index:'+pos.zIndex+';">'
            +'<object type="application/x-shockwave-flash" data="'+this.JQueryContent.children().attr('data-flash')+'" width="'+pos.width+'" height="'+pos.height+'">'
            +'</object></div>';
        }
    }
};

//-----------------------------------------------------------------------------------
//                          FUNCTIONS FOR EDITING ELEMENTS
//-----------------------------------------------------------------------------------

/**
 * Functions in element "menu" (Delete & edit). Each element has own edit function.
 */
Prototypes.MenuFunctions={
    /**
     * Edit content field & box element.
     */
    areaEdit:function(mainPage){
        
        var form=$.tmpl($('#content-field-edit'),{
            mainPage:mainPage
        }),
        
        el=this.JQueryContent;      
        
        form.appendBorderEditor(el);
        form.appendColorPicker(el);
        
        form.find('#field-height').val(parseInt(el.css('height')));
        
        form.find('#field-width').val(parseInt(el.css('width')));
        if(el.css('background-image').indexOf('none')==-1)
            form.find('#background-image').val(el.css('background-image').replace('url("','').replace('")','').replace('url(','').replace(')',''));
        if(mainPage){
            form.find('#keywords').val($('.panel-content').attr('data-keywords'));
            form.find('#trap').val($('.panel-content').attr('data-trap'));
            form.find('#favicon').val($('.panel-content').attr('data-favicon'))
            form.find('#title').val($('.panel-content').attr('data-title'))
            if($('.panel-content').attr('data-trap-active')=='true')
                form.find('#trap-active').attr('checked','true');
        }
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            
            var type=$(form.find('.buttons').children('.select')[0]).attr('data-border');
            
            if(mainPage){
                $('.setka').css({
                    'height':form.find('#field-height').val()+'px',
                    'width':form.find('#field-width').val()+'px'
                });
            }

            el.css({
                'background-color':(form.find('#bg-color-edit').first().val().trim()),
                'height':form.find('#field-height').val()+'px',
                'width':form.find('#field-width').val()+'px',
                'border':type+' '+form.attr('data-border-size')+' '+form.attr('data-border-color'),
                'background-image':form.find('#background-image').val().length>0?'url("'+form.find('#background-image').val().trim()+'")':'',
                'background-position':'fixed',
                'background-size':'100% 100%',
                'background-repeat': 'no-repeat'
            });
            if(mainPage){
                $('.panel-content').attr('data-keywords',form.find('#keywords').val());
                $('.panel-content').attr('data-trap',form.find('#trap').val());
                $('.panel-content').attr('data-favicon',form.find('#favicon').val());
                $('.panel-content').attr('data-title',form.find('#title').val());
                $('.panel-content').attr('data-trap-active',form.find('#trap-active').is(':checked'));
                var width_count = Math.ceil(parseInt(el.css('width'))/100);
                var height_count = Math.ceil(parseInt(el.css('height'))/100);
                var i =0;
                var width_count_str = '';
                while(i<=width_count){
                    i++;
                    width_count_str+='<span>'+i*100+'</span>';
                }
                el.find('.line_wight .body').html(width_count_str);
                
                
                
                var height_count_str = '';
                i=0;
                while(i<=height_count){
                    i++;
                    height_count_str+='<div><span>'+i*100+'</span></div>';
                }
                
                el.find('.line_height .body').html(height_count_str);

                el.children('.line_height').css({
                    'height':el.css('height')
                });
                el.children('.line_wight').css({
                    'width':el.css('width')
                });
            }
            
            form.remove();
        });
        
        form.showDialog(); 
    },
    /**
     * Delete element
     */
    deleteEl:function(){
        
        var form=$.tmpl($('#questions'),{
            question:'Вы действительно хотите удалить элемент?'
        }),
        self=this;
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            self.deleteEl();
            
            form.remove();
        });

        form.showDialog();
    },
    /**
     * Edit youtube
     */
    editYoutube:function(){
        var el=this.JQueryContent,
            youtubeRGX=/http:\/\/(?:www\.)?youtu(?:be\.com\/(?:watch\?v=|v\/)|\.be\/)([^&]+).*$/i,
            videoId=el.attr('src').split('/')[4],
            isAuto=false;
        
        var form=$.tmpl($('#video-edit'));
        if(el.attr('data-ruel-link'))
            form.find('.text').val(el.attr('data-ruel-link'));
        else
            form.find('.text').val('http://www.youtube.com/watch?v='+videoId);
        if(el.attr('data-autoplay'))
            form.find('#autoplay').attr('checked','true')
        form.fb('#autoplay','click',function(){
            isAuto=$(this).is(':checked')
        })
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var link=form.find('.text').val();
            if(isAuto)
                el.attr('data-autoplay','true')
            else
                el.removeAttr('data-autoplay')
            if(youtubeRGX.exec(link.trim())){
                el.attr('src','http://i.ytimg.com/vi/'+RegExp.$1+'/0.jpg');
                el.removeAttr('data-ruel-link');
                form.remove();
                return;
            }else if(/^http:\/\/storage\.ruelsoft\.org\/storage\/file\/get\/([0-9]+)\/{0,1}$/.test(link.trim())){
                el.attr('src','/static/img/elements/player.jpg');
                el.attr('data-ruel-link',link);
                form.remove();
                return;
            }else{
                $.tmpl($('#info-message'),{
                    input:false,
                    message:'Введённая вами ссылка, не является ссылкой на YouTube или Медяахранилище RuElSoft'
                })
                .showDialog();
            }
        });
        form.showDialog();
    },
    /**
     * Edit text element
     */
    editText:function(){
        
        var el=this.JQueryContent,
        form=$.tmpl($('#text-edit'));
        
        var instance;
        form.find('textarea').val(el.html());
        
        form.fb('#save-btn','click',function(){
           
            EventEmitter.getInstance().emit('contentChanged');
            el.html(instance.getData());
            el.css({
                width:'100%',
                height:'auto'
            })
            form.remove();
        });
        form.showDialog({
            beforCenter:function(){
                $('.dialog').css({
                    'min-width':'781px', 
                    'min-height':'447px'
                });
                if(CKEDITOR.instances['mail_text'])
                    delete CKEDITOR.instances['mail_text'];
                CKEDITOR.replace('text-editor');
                instance=CKEDITOR.instances['mail_text'];
                CKEDITOR.config.width = 740;
                
            }
        });
        
    },
    /**
     * Edit link element
     */
    editLink:function(){
        var el=this.JQueryContent,
        
        form=$.tmpl($('#link-edit')),
        link=form.find('#link'),
        css={
            font_style:'',
            font_weight:''
        };
        
        form.attr('data-border-size',el.css('border-bottom-width'));
        form.appendBorderEditor(el,link);
        
        
        form.appendColorPicker(el,link);
        
        //Link text  
        form
        .find('#link-text-edit')
        .val(el.text())
        .bind('keyup',function(){
            link.text($(this).val());
        });
        //Link href
        form
        .find('#link-edit')
        .val(el.attr('href'))
        .bind('keyup',function(){
            link.attr('href',$(this).val());
        });
        //Link text size    
        form
        .find('#text-height-edit')
        .val(parseInt(el.css('font-size')));
        
        if(el.attr('target')=='_blank')
            form.find('#blank').attr('checked','true');
        
        form.fb('#blank','click',function(){
            if($(this).attr('checked'))
                link.attr('target','_blank') 
            else
                link.attr('target','') 
        });
        
        form
        .fb('#row-up','click',function(){
            var val=parseInt(form.find('#text-height-edit').val())+1;
            form
            .find('#text-height-edit').val(val);
            link.css('font-size',val+'px');
        });
        
        link
        .attr('href',el.attr('href'))
        .text(el.text())
        .css({
            'color':el.css('color'),
            'backgroud-color':el.css('background-color'),
            'border':el.css('border-bottom-style')+' '+el.css('border-bottom-width')+' '+el.css('border-bottom-color'),
            'font-weight':el.css('font-weight'),
            'font-style':el.css('font-style'),
            'text-decoration':el.css('text-decoration')
        });
        
        if(parseInt(el.css('font-weight'))==700||el.css('font-weight').toString().indexOf('bold')>-1)
            form.find('#text-bold').addClass('select');
        
        if(el.css('text-decoration')=='underline')
            form.find('#decoration').attr('checked','true');
        
        form.fb('#decoration','click',function(){
            if($(this).attr('checked'))
                link.css('text-decoration','underline');
            else
                link.css('text-decoration','none');
        });
            
        if(el.css('font-style').indexOf('italic')>-1)
            form.find('#text-italic').addClass('select');
        
        form
        .fb('#text-height-edit','keyup',function(){
            link.css('font-size',$(this).val()+'px');
        });
        
        form.fb('#row-down','click',function(){
            var val=parseInt(form.find('#text-height-edit').val())-1;
            form.find('#text-height-edit').val(val);
            link.css('font-size',val+'px');
        });
        
        form.fb('#text-bold','click',function(){
            if(css.font_weight==700||css.font_weight.toString().indexOf('bold')>-1){
                css.font_weight=400;
                $(this).removeClass('select')
            }else{
                css.font_weight=700;
                $(this).addClass('select')
            }
            link.css('font-weight',css.font_weight);
        });
        
        form.fb('#text-italic','click',function(){
            
            if(css.font_style.indexOf('italic')>-1){
                css.font_style='normal';
                $(this).removeClass('select')
            }else{
                css.font_style='italic';
                $(this).addClass('select')
            }
            link.css('font-style',css.font_style);
            
        });
        
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            el.text(form.find('#link-text-edit').val());
            el.attr('href',form.find('#link-edit').val());
            var type=form.find('.buttons').children('.select').attr('data-border');
            el.css({
                'background-color':form.find('#bg-color-edit').val(),
                'color':form.find('#text-color-edit').val(),
                'font-size':form.find('#text-height-edit').val()+'px',
                'font-style':css.font_style,
                'font-weight':css.font_weight,
                'text-decoration':link.css('text-decoration'),
                'border':type+' '+form.attr('data-border-size')+' '+form.attr('data-border-color')
            });
            
            el.attr('target',link.attr('target'));
            form.remove();
        });
        form.showDialog();
    },
    /**
     * Edit image element
     */
    editImage:function(){
        var el=this.JQueryContent,
        self=this,
        form=$.tmpl($('#img-edit'));
        
        form.find('#img-src-edit').val(el.attr('src'));
        form.find('#link-href').val(el.attr('img-href')==undefined?'':el.attr('img-href'))
        if(el.attr('link-active')=='true')
            form.find('#is-link').attr('checked','true');
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var val=form.find('#img-src-edit').val();
            
            if(form.find('#is-link').is(':checked'))
                el.attr('link-active','true');
            else
                el.removeAttr('link-active')
            el.attr('img-href',form.find('#link-href').val())
            el.attr('src',val);
            el.css({
                'width':'auto',
                'height':'auto'
            });
            self.JQueryEL.css({
                'width':'auto',
                'height':'auto'
            });
            
            form.remove();
        //}
        
        });
        
        form.showDialog();
        
        EventEmitter.getInstance().once('img-uploaded', function(data){
            el.attr('src',data.link);
            form.remove();
        });
    },
    /**
     * Edit subscribe form element
     */
    editForm:function(){           
        var form=$.tmpl($('#subscribe-form-edit'),{textColorEdit:true})
            ,el=this.JQueryContent,
            self=this;
        form.appendColorPicker(el.find('.button'),null,self.JQueryContent);
        
        form.fb('input[type=checkbox]','click',function(){
            var box=$(this);
            if(box.is(':checked'))
                box.parent('div').addClass('select')
            else
                box.parent('div').removeClass('select');
        });
        
        el.find('tr').each(function(){
            var el=$(this);
            form.find('[name="'+el.attr('id')+'"]').attr('checked','true').parent('div').addClass('select').children('input[type=text]').val(el.find('input').attr('placeholder')); 
        });
        form.find('#text-height-edit').val(parseInt(el.css('font-size')));
        form.find('input[type="checkbox"]').each(function(){
            var el=$(this);
            if(!el.is(':checked')){
                el.parent().children('input[type="text"]').attr('disabled','disabled');
            }
        });
        if(GUI.getInstance().mailGroups){
            GUI.getInstance().mailGroups.map(function(el){
                $('<option value="'+el.Tag_CH+'">'+el.nameTag+'</option>').appendTo(form.find('#group-select'));
            });
        }
        
        if(el.attr('data-group-id'))
            form.find('#group-select').val(el.attr('data-group-id'));
        else
            form.find('#group-select').val(-1);
        
        form.fb('input[type="checkbox"]','click',function(){
            var el=$(this);
            if(el.is(':checked')){
                el.parent().children('input[type="text"]').removeAttr('disabled');
            }else{
                el.parent().children('input[type="text"]').attr('disabled','disabled');
            }
        });
        
        form.fb('#row-up','click',function(){
            var val=parseInt(form.find('#text-height-edit').val())+1;
            form.find('#text-height-edit').val(val);
        });
        
        form.fb('#row-down','click',function(){
            var val=parseInt(form.find('#text-height-edit').val())-1;
            form.find('#text-height-edit').val(val);
        });
        
        form.find('#btn-txt').val(el.find('.button').children('span').text());
        
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var type=form.find('.buttons').children('.select').attr('data-border');
            if(form.find('#group-select').val().indexOf('Без')>-1)
                el.removeAttr('data-group-id')
            else
                el.attr('data-group-id',form.find('#group-select').val());
            el.find('.button').children('span').text(form.find('#btn-txt').val());
            var width=el.find('input').css('width')
                ,height=el.find('input').css('height');
            el.find('tr').remove();
            
            form.find('.select').children('input[type=checkbox]').each(function(){
                var input=$(this);
                $('<tr id="'+input.attr('name')+'">'+
                    '<td><input placeholder="'+input.parent().children('input[type=text]').val()+'" type="text"/></td>'+
                    '</tr>')
                .appendTo(el.find('tbody'))
            });
            
            el.css({
                'border':type+' '+form.attr('data-border-size')+' '+form.attr('data-border-color'),
                'font-size':parseInt(form.find('#text-height-edit').val())+'px'
            });
            el.css('background-color',form.find('#bg-form-color-edit').val().trim());
            el.find('.button').css({
                'font-size':form.find('#text-height-edit').val(),
                'color':(form.find('#text-color-edit').first().val().trim()),
                'background-color':(form.find('#bg-color-edit').first().val().trim()),
                'border-color':(form.find('#bg-color-edit').first().val().trim())
            });
            
            self.JQueryEL.css({
                'width':'auto',
                'height':'auto'
            });
            form.remove();
        });
        form.appendBorderEditor(el);
        
        form.showDialog();
    },
    /**
     * Edit raw html code element
     */
    editHtml:function(){
        var el=this.JQueryContent,
        form=$.tmpl($('#edit-html'));
        
        form.find('textarea').val(el.html());
        
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var html=form.find('textarea').val();
            /**
             * For [FLASH] bb code
             */
            if(/\[flash\=([0-9,0-9,A-z\.\/\:]+)/.test(html)){
                var code=RegExp.$1.split(',');
                
                var img='<img src="/static/img/elements/flash.png" style="width:'
                +code[0]+'px;height:'
                +code[1]+'px" data-flash="'
                +code[2].substring(0,code[2].length-1)
                +'"/>';
                el.html(img);
                el.parent().parent().css({
                    width:code[0]+'px',
                    height:code[1]+'px'
                });
            }else{
                /**
                 * For other code
                 */
                if(html.indexOf('-flash')!=-1){

                    html=html.replace('</object>','<param name="wmode" value="opaque" /></object>');

                }
                var _el=$('<div>'+html+'</div>');
                
                el.html(_el.html());
                el.css({
                    width:'auto',
                    height:'auto'
                }).parent().parent()
                .css({
                    width:'auto',
                    height:'auto'
                })
            }
            
            form.remove();
        });
        
        form.showDialog();
    },
    /**
     * Edit audio element
     */
    editAudio:function(){
        
        var  el=this.JQueryEL,
        self=this,
        form=$.tmpl($('#edit-audio')),
        player_type;
        form.find('.text').val(el.children().children().attr('data-audio'));
        
        
        if(el.children().children().attr('data-onstart')=='true')
            form.find('#onstart').attr('checked','true');
        form.find('#player-type').val(el.attr('data-decoration-id')?el.attr('data-decoration-id'):1);
        form.find('#player-preview')
        .attr('src',form.find('#player-preview')
            .attr('src')
            .replace(/audio-view_s[0-9].jpg/,'audio-view_s'+(el.attr('data-decoration-id')?el.attr('data-decoration-id'):1)+'.jpg'));
        player_type=parseInt(form.find('#player-type').val());
        p(form.find('#player-type').val())
        form.fb('#player-type','change',function(){
            var src=$('#player-preview').attr('src');
            player_type=$(this).val();

            $('#player-preview').attr('src',src.replace(/audio-view_s[0-9].jpg/,'audio-view_s'+player_type+'.jpg'));
            
        });
        form.fb('#save-btn','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            el.children().children().attr('data-audio',form.find('.text').val());
            el.children().children().attr('data-onstart',form.find('#onstart').is(':checked'));
            
            switch(parseInt(player_type)){
                case 1:
                    self.JQueryContent.attr('data-decoration-id',1);
                    self.JQueryContent.attr('data-decoration','');
                    break;
                case 2:
                    self.JQueryContent.attr('data-decoration','bgcolor1=c9c8c8&amp;bgcolor2=717171&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=6B6966&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=6B6966" ');
                    self.JQueryContent.attr('data-decoration-id',2);
                    break;
                case 3:
                    self.JQueryContent.attr('data-decoration','bgcolor1=D4DE4E&amp;bgcolor2=63A45F&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=66A062&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=66A062"" ');
                    self.JQueryContent.attr('data-decoration-id',3);
                    break;
                case 4:
                    self.JQueryContent.attr('data-decoration','bgcolor1=008CD0&amp;bgcolor2=5B7BBB&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=3B5172&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=3B5172"" ');
                    self.JQueryContent.attr('data-decoration-id',4);
                    break;
                case 5:
                    self.JQueryContent.attr('data-decoration','bgcolor1=D65988&amp;bgcolor2=81589f&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=61447A&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=61447A"" ');
                    self.JQueryContent.attr('data-decoration-id',5);
                    break;
            }
            var img=el.find('#audio').children();
            var src=img.attr('src');
            if(src.indexOf('/static/img/elements/audio-view.png')!=-1){
                img.attr('src','/static/img/elements/'+'audio-view_s'+(player_type?player_type:1)+'.jpg');
            }else{
                img.attr('src',img
                    .attr('src')
                    .replace(/audio-view_s[0-9].jpg/,'audio-view_s'+(player_type?player_type:1)+'.jpg'));
            }
            form.remove();
        });
        
        form.showDialog();
    }
};
