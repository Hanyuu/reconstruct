# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.storage.views import StorageView

urlpatterns = patterns('',
                       url(r'storage/$', StorageView.as_view(), name='storage')
)