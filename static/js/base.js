
/***
 * This script not capture part!
 * This script include in generated page.
 */

$(document).ready(function(){
    var counter=$('#visit-counter')
		,socials=$('#social_network')
		,isLocalHref=false;
    if (socials.attr('id')=='social_network')    
        socials.html(socials.html().replace(/%1/gi,document.location.href).replace(/%2/gi,document.title));
    //We has counter, update it!
	$('a').live('click',function(){
            var el=$(this);
            isLocalHref=el.attr('href').indexOf('ruelsoft.')!=-1&&el.attr('target').indexOf('_blank')==-1;
	});
    if(counter.attr('id')=='visit-counter'){
        
        $.getJSON("http://demo.appsruel.com/capture/ajax/updatecounts/?callback=?",{
            user_id:$('#key').text(),
            page:location.pathname.replace(/\//gi,'')
        },function(data){
            var nums=data.counts.toString().split('');
            
            counter.find('span').hide(250);
            for(var i in nums){
                $('<span>'+nums[i]+'</span>').appendTo(counter.find('div')).show(250);
            }    
                
            
        });
    }
    $('.button').unbind('click').bind('click',function(){
        var form=$(this).parent().parent();
        var data={};
        form.find('input').each(function(){
            data[$(this).parent().parent().attr('id')]=$(this).val();
        });
        data['userid']=$('#key').text();
        data['captureid']='http://'+location.host+location.pathname;
        if(form.attr('data-group-id'))
            data['group']=form.attr('data-group-id')
        $.getJSON("http://demo.appsruel.com/mailsender/subscribers/action/newSubscription?callback=?",
            data,
            function(result){
                var title;
                switch(result.status){
                    case 'failed':
                        title='Произошла ошибка';
                        break;
                    case 'ok':
                        title='E-mail добавлен';
                        form.find('input').each(function(){
                            $(this).val('');
                        });
                        break;
                }
                $('<div>'+result.info+'</div>').dialog({
                    modal:true,
                    title: '<p class="text-title">'+title+'</p>',
                    zIndex:7000,
                    width:510,
                    resizable:false,
                    //height:578,
                    position:'center',
                    close:function(){
                        $(this).remove();
                    }
                });
            });
        
    });
    if ($('#ga-code').attr('id')!=undefined){
        (function(i,s,o,g,r,a,m){
            i['GoogleAnalyticsObject']=r;
            i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)
            },i[r].l=1*new Date();
            a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];
            a.async=1;
            a.src=g;
            m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', $('#ga-code').attr('data-ga-code'), 'ruelsoft.org');
        ga('send', 'pageview');
    }
    $(window).unbind('scroll').bind('scroll',function(e){
        //console.log(e); 
        });
    if($('#trap').attr('id')=='trap'){
        $(window).unbind('beforeunload').bind('beforeunload',function(e){
			if(isLocalHref)
				return;
            $(document.body).children('div').find('*').each(function(){
                var el=$(this);
                if((el.attr('id')==undefined&&el.parent().attr('id')==undefined)||el.attr('id')=='social_network'){
                    el.remove();
                }
               
            });
            $(document.body).find('object').each(function(){
				$(this).remove();
			});
            e.preventDefault?e.preventDefault():e.returnValue = false;
            $(document.body).css('height','').children('div').attr('style','');
            counter.remove();
            $('#trap').css({
                width:'100%',
                height:'100%',
                'z-index':99999
            });
            return '';
        });
        
    }
});